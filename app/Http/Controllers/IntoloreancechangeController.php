<?php

namespace App\Http\Controllers;
use App\Models\Intoloreance;
use Illuminate\Http\Request;

class IntoloreancechangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $into = Intoloreance::all();
        return view('intoloreance.index',compact('into'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('intoloreance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $item = new Intoloreance();
        $item->create($input);
        return redirect()->route('intoloreance.index')->with('created','Հաջողությամբ ավելացվել է');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $into = Intoloreance::find($id);
        return view('intoloreance.edit', compact('into'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token');
        $item = Intoloreance::findOrFail($id);
        $item->update($input);

        return redirect()->route('intoloreance.index',$id)->with('updated','Հաջողությամբ փոփոխվել է');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Intoloreance::find($id)->delete();
        return redirect()->route('intoloreance.index',$id)->with('deleted','Հաջողությամբ ջնջվել է');
    }
}
