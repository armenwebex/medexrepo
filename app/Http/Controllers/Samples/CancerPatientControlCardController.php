<?php

namespace App\Http\Controllers\Samples;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Patient;
use App\Models\ApplicationPurposeList;
use App\Models\CurrentStageList;
use App\Models\ExitList;
use App\Models\HistologicalList;
use App\Models\Metastasis_list;
use App\Models\ResearchesList;
use App\Models\PatientFirstInfo;
use App\Models\RegionList;
use App\Models\Samples\CancerPatientControlCard;

use App\Http\Requests\CancerPatientControlCardRequest;

use Illuminate\Support\Facades\Validator;

class CancerPatientControlCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * relation: hasOne
     * route-name: 'samples.patients.cpcc.index'
     * @return \Illuminate\Http\Response
     */
    public function index(Patient $patient)
    {
        // $cpcc = $patient->CancerPatientControlCard;
        $cpcc = $patient->CancerPatientControlCard()->onlyApproved()->get();
        return view('samples.cancer_patient_control_card.index', compact('cpcc', 'patient'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        if ($patient->CancerPatientControlCard->isNotEmpty()) {
            return redirect()->route('samples.patients.cpcc.index', $patient);
        }

        $repeatables = 5;
        $regions = RegionList::get();
        $first_info = PatientFirstInfo::where('patient_id', '=', $patient->id)->with('first_clinic_item', 'first_discovered_item')->get();
        $stationaries = $patient->stationaries;
        $cancer_groups = $patient->cancer_groups;
        $radiation_treatment_card = $patient->radiation_treatment_card;
        $histological_examinations = $patient->histological_examinations;
        $immunologicalExaminationPatternN7 = $patient->ImmunologicalExaminationPatternN7;

        return view('samples.cancer_patient_control_card.create2', compact(
            'patient',
            'repeatables',
            'stationaries',
            'first_info',
            'histological_examinations',
            'regions',

            'immunologicalExaminationPatternN7',
            'cancer_groups',
            'radiation_treatment_card'
        ));
    }

    public function create3(Patient $patient)
    {
        $regions = RegionList::get();
        return view('samples.cancer_patient_control_card.create3', compact('regions'));
    }

    /**
     * Store a newly created resource in storage.
     * route name: samples.patients.cpcc.store
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Patient $patient, CancerPatientControlCardRequest $request)
    {
        $validated = $request->validated();
        $data = $validated; // $request->all();
        $data['patient_id'] = $patient->id;
        unset($data['research_list_ids']);
        $cancer_patient_control_card = CancerPatientControlCard::create($data);

        // if ($request->filled('research_list_ids')) {
        if (isset($validated['research_list_ids'])) {
            $research_list_ids_array = explode(',', $request->research_list_ids);
            $cancer_patient_control_card->researches()->attach($research_list_ids_array);
        }

        return response()->json([
            'success' => __('ambulator.saved'),
            'redirect' => route('samples.patients.cpcc.index', $patient)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cpcc = CancerPatientControlCard::find($id)->first();
        $patient  = Patient::find($cpcc->patient_id);
        $first_info = PatientFirstInfo::where('patient_id', '=', $patient->id)->with('first_clinic_item', 'first_discovered_item')->get();
        $stationaries = $patient->stationaries;
        return view('samples.cancer_patient_control_card.show',compact('cpcc','patient','stationaries','first_info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient, $id)
    {
        $card = CancerPatientControlCard::where('patient_id', $patient->id)->findOrFail($id);
        // $patient = Patient::find($card->patient_id);
        $regions = RegionList::get();
        $first_info = PatientFirstInfo::where('patient_id', '=', $patient->id)->with('first_clinic_item', 'first_discovered_item')->get();
        $stationaries = $patient->stationaries;
        $cancer_groups = $patient->cancer_groups;
        $radiation_treatment_card = $patient->radiation_treatment_card;
        $histological_examinations = $patient->histological_examinations;
        $immunologicalExaminationPatternN7 = $patient->ImmunologicalExaminationPatternN7;

        return view('samples.cancer_patient_control_card.edit', compact(
            'card',
            'patient',
            'regions',
            'first_info',
            'stationaries',
            'cancer_groups',
            'radiation_treatment_card',
            'histological_examinations',
            'immunologicalExaminationPatternN7'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CancerPatientControlCardRequest $request, $id)
    {
        // id-parameter is patient's id
        $validated = $request->validated();
        $data = $validated;
        unset($data['research_list_ids']);
        unset($data['cpcc_id']);
        if ($validated['primary_multiple_malignant_neoplasm_deployment'] < 1) {
            $data['synchronous'] = null;
        }
        $cancer_patient_control_card = CancerPatientControlCard::where('patient_id', $id)->findOrFail($request->cpcc_id);
        $cancer_patient_control_card->update($data);

        if (isset($validated['research_list_ids'])) {
            $research_list_ids_array = explode(',', $request->research_list_ids);
            $cancer_patient_control_card->researches()->sync($research_list_ids_array);
        }

        return response()->json([
            'success' => __('ambulator.saved'),
            // 'redirect' => route('samples.patients.cpcc.index', $patient)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
