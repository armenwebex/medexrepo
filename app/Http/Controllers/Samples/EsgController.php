<?php

namespace App\Http\Controllers\Samples;
use App\Http\Controllers\Controller;
use App\Models\Samples\Esg;
use App\Models\Patient;
use App\Models\Approvement;
use Illuminate\Http\Request;

class EsgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Patient $patient)
    {
        $esgs = $patient->esgs()->onlyApproved()->with("attending_doctor")->get();
        return view("samples.esg.index")->with(compact("esgs", "patient"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        return view('samples.esg.create')->with(compact('patient'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        $data = $request->all();
        $data['patient_id'] = $patient->id;
        Esg::create($data);
        return redirect()->back()->with('success', 'Հաջողությամբ ավելացվել է');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient, $em_id)
    {
        $em = $patient->esgs()->onlyApproved()->findOrFail($em_id);

        return view("samples.esg.show")->with(compact('patient', 'em'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient, $em_id)
    {

        $em = $patient->esgs()->findOrFail($em_id);
        $this->authorize("belongs-to-user", $em);

        return view("samples.esg.edit")->with(compact('patient', 'em'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function update(Patient $patient,Request $request,$em_id)
    {
        $em = $patient->esgs()->findOrFail($em_id);

        $this->authorize("belongs-to-user", $em);

        $em->update($request->all());

        return redirect()->back()->with('updated', 'Հաջողությամբ թարմացվել է');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function destroy($patent_id,$id)
    {
        $esg = Esg::where('patient_id',$patent_id)->where('user_id',auth()->id())->find($id);
        $approvement=Approvement::where('approvable_id',$id)->delete();

        if ($esg==null){
            abort('404');
        }
        $esg->delete();
        return back()->with('ok','colums delete');
    }
}
