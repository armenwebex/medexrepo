<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\HistologicalStructureList;
use Illuminate\Http\Request;

class HistologicalStructureListController extends Controller
{
    public function histological_structure_lists_json(
        Request $request,
        HistologicalStructureList $histological_structure_list
    ) {
        return response()->json($histological_structure_list->search($request->q ?? ""));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HistologicalStructureList  $histologicalStructureList
     * @return \Illuminate\Http\Response
     */
    public function show(HistologicalStructureList $histologicalStructureList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HistologicalStructureList  $histologicalStructureList
     * @return \Illuminate\Http\Response
     */
    public function edit(HistologicalStructureList $histologicalStructureList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HistologicalStructureList  $histologicalStructureList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HistologicalStructureList $histologicalStructureList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HistologicalStructureList  $histologicalStructureList
     * @return \Illuminate\Http\Response
     */
    public function destroy(HistologicalStructureList $histologicalStructureList)
    {
        //
    }
}
