<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{

    /**
     *  Return json list of patients
     *
     * @return \Illuminate\Http\Response
     */
    public function patients_json()
    {
        $patients = Patient::select('id', 'f_name', 'l_name', 'p_name','soc_card')->get(); // auto generate all_names by $appends
        return response()->json($patients);
    }
}
