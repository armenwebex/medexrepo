<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\RegionList;
use Illuminate\Http\Request;

class RegionListController extends Controller
{

    public function region_lists_json(Request $request, RegionList $region_list)
    {
        // return response()->json($region_list);
        return response()->json($region_list->search($request->q ?? ""));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegionList  $regionList
     * @return \Illuminate\Http\Response
     */
    public function show(RegionList $regionList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegionList  $regionList
     * @return \Illuminate\Http\Response
     */
    public function edit(RegionList $regionList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegionList  $regionList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegionList $regionList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegionList  $regionList
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegionList $regionList)
    {
        //
    }
}
