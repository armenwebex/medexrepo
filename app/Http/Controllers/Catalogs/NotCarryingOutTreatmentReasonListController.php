<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\NotCarryingOutTreatmentReasonList;
use Illuminate\Http\Request;

class NotCarryingOutTreatmentReasonListController extends Controller
{

    public function not_carrying_out_treatment_reason_lists_json(
        Request $request,
        NotCarryingOutTreatmentReasonList $not_carrying_out_treatment_reason_list
    ) {
        return response()->json($not_carrying_out_treatment_reason_list->search($request->q ?? ""));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NotCarryingOutTreatmentReasonList  $notCarryingOutTreatmentReasonList
     * @return \Illuminate\Http\Response
     */
    public function show(NotCarryingOutTreatmentReasonList $notCarryingOutTreatmentReasonList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NotCarryingOutTreatmentReasonList  $notCarryingOutTreatmentReasonList
     * @return \Illuminate\Http\Response
     */
    public function edit(NotCarryingOutTreatmentReasonList $notCarryingOutTreatmentReasonList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NotCarryingOutTreatmentReasonList  $notCarryingOutTreatmentReasonList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotCarryingOutTreatmentReasonList $notCarryingOutTreatmentReasonList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NotCarryingOutTreatmentReasonList  $notCarryingOutTreatmentReasonList
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotCarryingOutTreatmentReasonList $notCarryingOutTreatmentReasonList)
    {
        //
    }
}
