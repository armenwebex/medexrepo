<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\RegistrationOptionList;
use Illuminate\Http\Request;

class RegistrationOptionListController extends Controller
{

    public function registration_option_list_json(Request $request, RegistrationOptionList $registration_option_list)
    {
        return response()->json($registration_option_list->search($request->q ?? ""));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegistrationOptionList  $registrationOptionList
     * @return \Illuminate\Http\Response
     */
    public function show(RegistrationOptionList $registrationOptionList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegistrationOptionList  $registrationOptionList
     * @return \Illuminate\Http\Response
     */
    public function edit(RegistrationOptionList $registrationOptionList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegistrationOptionList  $registrationOptionList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistrationOptionList $registrationOptionList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegistrationOptionList  $registrationOptionList
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistrationOptionList $registrationOptionList)
    {
        //
    }
}
