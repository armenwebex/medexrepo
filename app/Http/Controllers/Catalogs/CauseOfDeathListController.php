<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CauseOfDeathList;

class CauseOfDeathListController extends Controller
{
    public function cause_of_death_lists_json( Request $request, CauseOfDeathList $cause_of_death_list)
    {
        return response()->json($cause_of_death_list->search($request->q ?? ""));
    }
}
