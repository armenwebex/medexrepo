<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\ApplicationPurposeList;
use Illuminate\Http\Request;

class ApplicationPurposeListController extends Controller
{
    public function application_purpose_list_json(Request $request, ApplicationPurposeList $application_purpose_list)
    {
        return response()->json($application_purpose_list->search($request->q ?? ""));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ApplicationPurposeList  $applicationPurposeList
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationPurposeList $applicationPurposeList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ApplicationPurposeList  $applicationPurposeList
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplicationPurposeList $applicationPurposeList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ApplicationPurposeList  $applicationPurposeList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplicationPurposeList $applicationPurposeList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ApplicationPurposeList  $applicationPurposeList
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationPurposeList $applicationPurposeList)
    {
        //
    }
}
