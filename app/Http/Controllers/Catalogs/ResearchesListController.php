<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\ResearchesList;
use Illuminate\Http\Request;

class ResearchesListController extends Controller
{
    public function researches_lists_json(Request $request, ResearchesList $researches_list)
    {
        return response()->json($researches_list->search($request->q ?? ""));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ResearchesList  $researchesList
     * @return \Illuminate\Http\Response
     */
    public function show(ResearchesList $researchesList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ResearchesList  $researchesList
     * @return \Illuminate\Http\Response
     */
    public function edit(ResearchesList $researchesList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ResearchesList  $researchesList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResearchesList $researchesList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ResearchesList  $researchesList
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchesList $researchesList)
    {
        //
    }
}
