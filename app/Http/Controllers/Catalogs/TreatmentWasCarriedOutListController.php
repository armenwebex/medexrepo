<?php

namespace App\Http\Controllers\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\TreatmentWasCarriedOutList;
use Illuminate\Http\Request;

class TreatmentWasCarriedOutListController extends Controller
{

    public function treatment_was_carried_out_lists_json(
        Request $request,
        TreatmentWasCarriedOutList $treatment_was_carried_out_list
    ) {
        return response()->json($treatment_was_carried_out_list->search($request->q ?? ""));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TreatmentWasCarriedOutList  $treatmentWasCarriedOutList
     * @return \Illuminate\Http\Response
     */
    public function show(TreatmentWasCarriedOutList $treatmentWasCarriedOutList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TreatmentWasCarriedOutList  $treatmentWasCarriedOutList
     * @return \Illuminate\Http\Response
     */
    public function edit(TreatmentWasCarriedOutList $treatmentWasCarriedOutList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TreatmentWasCarriedOutList  $treatmentWasCarriedOutList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TreatmentWasCarriedOutList $treatmentWasCarriedOutList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TreatmentWasCarriedOutList  $treatmentWasCarriedOutList
     * @return \Illuminate\Http\Response
     */
    public function destroy(TreatmentWasCarriedOutList $treatmentWasCarriedOutList)
    {
        //
    }
}
