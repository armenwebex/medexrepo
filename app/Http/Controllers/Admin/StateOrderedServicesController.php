<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StateOrderedService;
use Illuminate\Http\Request;

class StateOrderedServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = StateOrderedService::all();
        return  view('admin.state-ordered-services.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.state-ordered-services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required|unique',
            'cost' => 'required',
        ]);


        StateOrderedService::create($request->all());
        return redirect()->route('admin.state-ordered-services.index')->with('msg','ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = StateOrderedService::find($id);
        return view('admin.state-ordered-services.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->name != '') {
            StateOrderedService::find($id)->update([
                'name' => $request->name,
                'code' => $request->code,
                'measurement_unite' => $request->measurement_unit,
                'cost' => $request->cost,
                'status' => $request->status
            ]);
        } else {
            if ($request->status == 'active') {
                StateOrderedService::find($id)->update([
                    'status' => 'inactive'
                ]);
            } else {
                StateOrderedService::find($id)->update([
                    'status' => 'active'
                ]);
            }

        }
        return redirect()->route('admin.state-ordered-services.index')->with('success','Թարմացվեց');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
