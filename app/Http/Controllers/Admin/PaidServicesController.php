<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaidService;
use Illuminate\Http\Request;

class PaidServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = PaidService::all();
        return  view('admin.paid-services.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.paid-services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
            'cost' => 'required',
        ]);

        PaidService::create($request->all());
        return redirect()->route('admin.paid-services.index')->with('msg','ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = PaidService::find($id);
        return view('admin.paid-services.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->name != '') {
            PaidService::find($id)->update([
                'name' => $request->name,
                'code' => $request->code,
                'key_one' => $request->key_one,
                'key_two' => $request->key_two,
                'cost' => $request->cost,
                'status' => $request->status
            ]);
        } else {
            if ($request->status == 'active') {
                PaidService::find($id)->update([
                    'status' => 'inactive'
                ]);
            } else {
                PaidService::find($id)->update([
                    'status' => 'active'
                ]);
            }

        }
        return redirect()->route('admin.paid-services.index')->with('success','Թարմացվեց');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
