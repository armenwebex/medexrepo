<?php

namespace App\Http\Controllers;

use App\Esg;
use Illuminate\Http\Request;

class EsgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('samples.esg.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('samples.esg.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('samples.esg.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Esg $esg)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Esg  $esg
     * @return \Illuminate\Http\Response
     */
    public function destroy(Esg $esg)
    {
        //
    }
}
