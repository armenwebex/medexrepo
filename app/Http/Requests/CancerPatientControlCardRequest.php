<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CancerPatientControlCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attached_clinic_id' => 'required|numeric',

            'age_at_discovery_year' => 'nullable|numeric', // is nullable for update
            'age_type_at_discovery_year' => 'nullable|string', // is nullable for update
            'age_code_at_discovery_year' => 'nullable|numeric', // is nullable for update

            'primary_multiple_malignant_neoplasm_deployment' => 'required|boolean',
            'synchronous' => 'nullable|boolean',

            'application_purpose_id' => 'required|numeric',
            'cpcc_registration_date' => 'required|date|before:tomorrow',
            'registration_option_id' => 'required|numeric',

            'research_list_ids' => 'required|string',

            'tumor_visible_location' => 'required|boolean',

            'not_carrying_out_treatment_reason_date' => 'required|date|before:tomorrow',
            'not_carrying_out_treatment_reason_id' => 'required|numeric',

            'treatment_was_carried_out_date' => 'required|date|before:tomorrow',
            'treatment_was_carried_out_id' => 'required|numeric',

            'diagnosis_hasnot_confirmed_date' => 'required|date|before:tomorrow',

            'cpcc_id' => 'nullable|numeric|exists:cancer_patient_control_cards,id' // is nullable for create
        ];
    }
}
