<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roomtypes extends Model
{
    public $table = 'roomtypes';
    protected $fillable = ['name'];
}
