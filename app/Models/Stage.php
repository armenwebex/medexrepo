<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    public $table = 'stage';
    protected $fillable = ['name'];
}
