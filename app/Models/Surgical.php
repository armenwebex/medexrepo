<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Surgical extends Model
{
    protected $table = 'surgical';
    protected $fillable = ['code',"name",'status', 'created_at', 'updated_at'];
}
