<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCacheableOptions;

class RegistrationOptionList extends Model
{
    use HasCacheableOptions;

    protected $fillable = ["name", 'status'];

    protected static function getColumnsForOptions(): array
    {
        return ["id as value", "name as label"];
    }
}
