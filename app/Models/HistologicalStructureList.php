<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

use App\Models\Samples\HistologicalExamination;

use App\Traits\HasCacheableOptions;

class HistologicalStructureList extends Model
{
    use HasCacheableOptions;
    protected $fillable = ['name', 'code', 'status'];

    public function histological_examinations(): BelongsToMany
    {
        return $this->belongsToMany(HistologicalExamination::class);
        // pivot , 'histological_examination_histological_structure_list'
    }
}
