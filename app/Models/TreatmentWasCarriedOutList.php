<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCacheableOptions;
use App\Models\Samples\CancerPatientControlCard;
use Illuminate\Database\Eloquent\Relations\HasOne;


class TreatmentWasCarriedOutList extends Model
{
    use HasCacheableOptions;
    protected $fillable = ['code', 'name', 'status'];

    public function cancer_patient_control_card(): HasOne
    {
        return $this->hasOne(CancerPatientControlCard::class);
    }

}
