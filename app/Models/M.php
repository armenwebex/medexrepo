<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M extends Model
{
    public $table = 'm';
    protected $fillable = ['name'];
}
