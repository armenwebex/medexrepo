<?php

namespace App\Models;

use App\Models\Samples\AdviceSheetInsurance;
use App\Traits\HasCacheableOptions;
use Illuminate\Database\Eloquent\Model;

use App\Models\Samples\CancerPatientControlCard;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ResearchesList extends Model
{
    use HasCacheableOptions;

    protected $fillable = ['code', 'name', 'status'];

    protected static function getColumnsForOptions(): array
    {
        return ["id as value", "name as label"];
    }

    public function control_cards(): BelongsToMany
    {
        return $this->belongsToMany(
            CancerPatientControlCard::class,
            'cpcc_researches',      // table-name
            'research_list_id',    // f-key
            'cpcc_id'               // rel-key
        );
    }
    public function advice_sheet_insurance(): BelongsToMany
    {
        return $this->belongsToMany(
            AdviceSheetInsurance::class,
            'advice_sheet_insurance_researches_lists',      // table-name
            'researches_id',    // f-key
            'a_s_i_id'   // rel-key
        );
    }
}
