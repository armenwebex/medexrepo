<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

use App\Traits\HasCacheableOptions;
use App\Models\Samples\CancerPatientControlCard;

class NotCarryingOutTreatmentReasonList extends Model
{
    use HasCacheableOptions;
    protected $fillable = ['code', 'name', 'status'];

    public function cancer_patient_control_card(): HasOne
    {
        return $this->hasOne(CancerPatientControlCard::class);
    }
}
