<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCacheableOptions;

use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\StationaryPathologicalAnatomical;

class CauseOfDeathList extends Model
{
    use HasCacheableOptions;
    protected $fillable = ['code', 'name', 'status'];

    /**
     * Get the stationary_pathological_anatomical associated with the CauseOfDeathList
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stationary_pathological_anatomical(): HasOne
    {
        return $this->hasOne(StationaryPathologicalAnatomical::class, 'cause_of_death', 'id');
    }
}
