<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCacheableOptions;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\Clinic;
use Illuminate\Database\Eloquent\Relations\HasOne;

class RegionList extends Model
{
    use HasCacheableOptions;

    protected $fillable = ["name", 'status', 'code']; // code used into clinics "region_code"

    protected static function getColumnsForOptions(): array
    {
        return ["id as value", "name as label", "code"];
    }

    public function clinics(): HasMany
    {
        return $this->hasMany(Clinic::class, 'regional_code', 'code',);
    }
}
