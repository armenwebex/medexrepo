<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chemotherapy extends Model
{
    protected $table = 'chemotherapy';
    protected $fillable = ['code',"name",'status', 'created_at', 'updated_at'];
}
