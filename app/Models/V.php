<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V extends Model
{
    public $table = 'v';
    protected $fillable = ['name'];
}
