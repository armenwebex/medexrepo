<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class T extends Model
{
    public $table = 't';
    protected $fillable = ['name'];
}
