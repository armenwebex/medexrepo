<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Radiation extends Model
{
    protected $table = 'radiation';
    protected $fillable = ['code',"name",'status', 'created_at', 'updated_at'];
}
