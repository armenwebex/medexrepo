<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instrumentalresearches extends Model
{
    protected $table = 'instrumentalresearches';
    protected $fillable = ["name",'status', 'created_at', 'updated_at'];
}
