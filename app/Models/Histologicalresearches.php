<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Histologicalresearches extends Model
{
    protected $table = 'histologicalresearches';
    protected $fillable = ["name",'status', 'created_at', 'updated_at'];
}
