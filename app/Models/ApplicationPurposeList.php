<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCacheableOptions;

class ApplicationPurposeList extends Model
{
    use HasCacheableOptions;

    protected static function getColumnsForOptions(): array
    {
        return ["id as value", "name as label"];
    }

    protected $fillable = ['name', 'status'];
}
