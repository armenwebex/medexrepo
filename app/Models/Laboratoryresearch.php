<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Laboratoryresearch extends Model
{
    protected $table = 'laboratoryresearches';
    protected $fillable = ["name",'status', 'created_at', 'updated_at'];
}
