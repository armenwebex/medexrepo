<?php

namespace App\Models;

use App\Traits\HasCacheableOptions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\RegionList;

class Clinic extends Model
{
    use HasCacheableOptions;

    public $timestamps = false;

    protected $fillable = ['name', 'code', 'regional_code', 'status'];
    protected $with = ['region'];

    protected static function getColumnsForOptions(): array
    {
        return ["id as value", "name as label", "regional_code"];
    }

    protected $casts = [
        "value" => "string",
    ];

    public function region(): BelongsTo
    {
        return $this->belongsTo(RegionList::class, 'regional_code', 'code');
    }
}
