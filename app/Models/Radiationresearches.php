<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Radiationresearches extends Model
{
    protected $table = 'radiationresearches';
    protected $fillable = ["name",'status', 'created_at', 'updated_at'];
}
