<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddCashbox extends Model
{
    protected $table = 'add_cashbox';
    protected $fillable = ['cassa_name','ip','hvhh','hdm_number'];
}
