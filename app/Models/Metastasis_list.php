<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metastasis_list extends Model
{
    protected $fillable = [
        'name',
        'code',
        'status'
    ];
}
