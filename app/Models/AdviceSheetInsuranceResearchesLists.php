<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdviceSheetInsuranceResearchesLists extends Model
{
    protected $table = "advice_sheet_insurance_researches_lists";
    protected $fillable = ['researches_id', 'a_s_i_id'];
}
