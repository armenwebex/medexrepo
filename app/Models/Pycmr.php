<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pycmr extends Model
{
    public $table = 'pycmr';
    protected $fillable = ['name'];
}
