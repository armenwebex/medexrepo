<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class N extends Model
{
    public $table = 'n';
    protected $fillable = ['name'];
}
