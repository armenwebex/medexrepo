<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class L extends Model
{
    public $table = 'l';
    protected $fillable = ['name'];
}
