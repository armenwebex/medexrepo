<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Intoloreance extends Model
{
    public $table = 'intoloreance';
    protected $fillable = ['id','name'];

    public function stationary_medicine_side_effects(): HasMany
    {
        return $this->hasMany("App\Models\StationaryMedicineSideEffect", "medicine_id", "id");
    }

}
