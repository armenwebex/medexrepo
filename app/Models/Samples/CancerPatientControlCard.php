<?php

namespace App\Models\Samples;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUserId;
use App\Traits\Approvable;
use App\Contracts\Models\Approvable as ApprovableContract;
use Spatie\Activitylog\Traits\LogsActivity;

use App\Models\User;
use App\Models\Clinic;
use App\Models\Patient;
use App\Models\ResearchesList;
use App\Models\ApplicationPurposeList;
use App\Models\RegistrationOptionList;
use App\Models\TreatmentWasCarriedOutList;
use App\Models\NotCarryingOutTreatmentReasonList;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CancerPatientControlCard extends Model implements ApprovableContract
{
    use HasUserId, LogsActivity, Approvable;

    protected static $logName = 'cancer_patient_control_card';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];

    protected $fillable = [
        'attached_clinic_id', // Պացիենտի կցագրում - բուժ․ հաստատության id-ն

        'age_at_discovery_year',    //  Տարիքը հայտնաբերման տարում
        'age_type_at_discovery_year',   // Տարիքի տիպը հայտնաբերման տարում /օրեկան/ամսեկան/տարեկան
        'age_code_at_discovery_year',   //  Տարիքային կոդ հայտնաբերման տարում

        'primary_multiple_malignant_neoplasm_deployment',   // Առաջնակի բազմակի տեղակայման չարորակ նորագոյացություն
        'synchronous',  // Միաժամանակյա (Սինքրոն) || Ոչ միաժամանակյա (Մետաքրոն)

        'application_purpose_id',   // Դիմելու նպատակը
        'cpcc_registration_date',    //Հաշվառում ՈՒԱԿ-ում
        'registration_option_id',   // Հաշվառման վերցնելու տարբերակները

        'research_list_ids', // json with casts Ինչ հետազոտությամբ է հաստատվել ախտորոշումը - relation-items

        'tumor_visible_location', // Ուռուցքի տեսանելի տեղակայում
        'not_carrying_out_treatment_reason_id', // Բուժումը չիրականացնելու պատճառը
        'not_carrying_out_treatment_reason_date', // Բուժումը չիրականացնելու պատճառը նշելու ամսաթիվ

        'treatment_was_carried_out_id',     // Բուժումը իրականացվել է այս կերպ
        'treatment_was_carried_out_date',   // Բուժումը իրականացվել է այս ամսաթվին

        'diagnosis_hasnot_confirmed_date',  // Ախտորոշմը չի հաստատվել



        'user_id',  // HasUserId
        'patient_id', // comes with request


    ];

    protected $with = [
        'researches',
        'patient',
        'user',
        'attached_clinic',
        'application_purpose',
        'registration_option',
        'not_carrying_out_treatment_reason',
        'treatment_was_carried_out'
    ];

    public function not_carrying_out_treatment_reason(): BelongsTo
    {
        return $this->belongsTo(NotCarryingOutTreatmentReasonList::class);
    }

    public function treatment_was_carried_out(): BelongsTo
    {
        return $this->belongsTo(TreatmentWasCarriedOutList::class);
    }

    public function researches(): BelongsToMany
    {
        return $this->belongsToMany(
            ResearchesList::class,
            'cpcc_researches',  // table-name
            'cpcc_id',          // f-key
            'research_list_id' // rel-key
        );
    }

    /**
     * Get the user that owns the CancerPatientControlCard
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the patient that owns the CancerPatientControlCard
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * Get the "attached-clinic" that owns the CancerPatientControlCard
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attached_clinic(): BelongsTo
    {
        return $this->belongsTo(Clinic::class, 'attached_clinic_id', 'id');
    }

    /**
     * Get the "application-purpose" that owns the CancerPatientControlCard
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function  application_purpose(): BelongsTo
    {
        return $this->belongsTo(ApplicationPurposeList::class, 'application_purpose_id', 'id');
    }

    /**
     * Get the "registration-option" that owns the CancerPatientControlCard
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function  registration_option(): BelongsTo
    {
        return $this->belongsTo(RegistrationOptionList::class);
    }
}
