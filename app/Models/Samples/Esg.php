<?php

namespace App\Models\Samples;

use App\Contracts\Models\Approvable as ApprovableContract;
use App\Traits\Approvable;
use App\Traits\HasUserId;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model;

class Esg extends Model implements ApprovableContract
{

    use Approvable,HasUserId;
    public $table = 'esgs';
    protected $fillable = [
        'electric',
        'history',
        'user_id',
        'ambulator',
        'date',
        'pq',
        'qrs',
        'qrst',
        'rr',
        'rhythm',
        'sistolik',
        'sistolikritm',
        'p',
        'r',
        'q',
        's',
        'st',
        't',
        'voltage',
        'inspiration',
        'note',
        'patient_id',
        'attending_doctor_id',
    ];

    public function patient(): BelongsTo
    {
        return $this->belongsTo("App\Models\Patient");
    }
    public function attending_doctor(): BelongsTo
    {
        return $this->belongsTo("App\Models\User", 'attending_doctor_id', 'id');
    }

}

