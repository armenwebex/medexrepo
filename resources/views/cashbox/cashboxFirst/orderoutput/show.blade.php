    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{mix('css/print/orderoutput.css')}}">
    <title>ԴՐԱՄԱՐԿՂԱՅԻՆ ԵԼՔԻ ՕՐԴԵՐ</title>
    @isset ($for_pdf)
        <style>
            *{ font-family: DejaVu Sans !important;}
        </style>
    @endif
</head>
<body>

<div class="page-wrap">
    <div class="new-page">
        <div class="main-container">
            <div>ՀՀ ԱՆ Վ.Ա. Ֆանարջյան անվան <br>
                Ուռուցքաբանության Ազքային <br>
                Կենտրոն ՓԲԸ
            </div>
            <div class="float-right">
                <div class="display-flex text-center"><br>
                    <div class="mt-1">Կոդ ըստ ՕԿՈՒԴ</div>
                    <div class="border">55555555 5</div>
                </div>
            </div>
            <br><br><br>
              <div class="text-center">
                  <div>ԵԼՔԻ ԴՐԱՄԱՐԿՂԱՅԻՆ ՕՐԴԵՐ N<span class="bottom-line">5</span></div>
              </div>
            <br>
                <table class="table text-center">
                    <tr>
                        <th>
                            Ամսաթիվ
                        </th>
                    </tr>
                    <tr>
                        <td>
                            10-10-2020
                        </td>
                    </tr>
                </table>
            <br>
            <table class="table text-center">
                <tr>
                    <th>
                        Թղթակցական հաշիվ,<br>
                        ենթահաշիվ
                    </th>
                    <th>
                        Վերլուծական<br>
                        հաշվարկի կոդը
                    </th>
                    <th>
                        Գումարը
                    </th>
                    <th>
                        Նպատակային <br>
                        նշանակման կոդ
                    </th>
                </tr>
                <tr>
                    <td>
                        10-10-2020
                    </td>
                    <td>
                        10-10-2020
                    </td>
                    <td>
                        {{ $order->price }}
                    </td>
                    <td>
                        10-10-2020
                    </td>
                </tr>
            </table>
            <br>
            <div class="display-flex">
                <div>Վճարել (Ա.Ա.Հ.)</div>
                <span class="bottom-line">
                    {{ $order->patient->f_name }} &nbsp {{ $order->patient->l_name }} &nbsp {{ $order->patient->p_name }}
                </span>
            </div>
            <br>
            <div class="display-flex">
                <div>Հիմք</div>
                <span class="bottom-line">Vjarovi</span>
            </div>
            <br>
            <div class="display-flex">
                <div>Գումար (բառերով)</div>
                <span class="bottom-line">{{ $order->sum_text }}</span>&nbsp; դ.
            </div>
            <br>
            <div class="display-flex">
                <div>Առդիր</div>
                <span class="bottom-line"></span>
            </div>
            <br>
            <div class="display-flex">
                <div class="display-flex">
                    <div>Ղեկավար</div>
                    <span class="bottom-line">Nikol Pashinyan</span>
                </div>
                <div class="display-flex margin-left">
                    <div>Գլխավոր հաշվապահ</div>
                    <span class="bottom-line">Nikol Pashinyan</span>
                </div>
            </div>
            <br>
            <div class="display-flex">
                <div>Ստացա(բառերով)</div>
                <span class="bottom-line">hing hazar</span> &nbsp; դ.
            </div>
            <br>
            <div class="display-flex">
                <div class="display-flex">
                    <div>Ամսաթիվ</div>
                    <span class="bottom-line">{{ $order->created_at->format('m/d/Y')    }}</span>
                </div>
                <div class="display-flex margin-left">
                    <div>Ստորագրություն</div>
                    <span class="bottom-line1"></span>
                </div>
            </div>
            <br>
            <div class="display-flex">
                <div>Ըստ`</div>
                <span class="bottom-line2">{{ $order->document_type }}</span>
            </div>
            <div class="text-center">
             <small>(Ստացողի անձը հաստատող փաստաթղթի անվանումը)</small>
            </div>
            <br>
            <p>{{ $order->passport_data }}</p>
            <div class="text-center">
                <small>(համարը,ամսաթիվը և հանձման վայրը)</small>
            </div>
            <br>
            <div class="display-flex">
                <div>Հանձնեց գանձապահ`</div>
                <span class="bottom-line">Arusik Tigranyan</span>
            </div>
        </div>
    </div>
</div>
</body>
</html>

{{--//Իսկական--}}
{{--@extends('layouts.cardBase')--}}

{{--@section('css')--}}
{{--    <link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />--}}
{{--    <link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet" />--}}
{{--@endsection--}}

{{--@section('card-header')--}}
{{--    Դրամարկղ--}}
{{--@endsection--}}

{{--@section('card-content')--}}


{{--    <form method="#" action="#">--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Id</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->id }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Ամսաթիվ</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->created_at }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Գումար (ՀՀ դրամ թվերով)</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->price }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Գումար (ՀՀ դրամ տառերով)</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->sum_text }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Պացիենտ</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->patient->f_name }}&nbsp;{{ $order->patient->l_name }}&nbsp;{{ $order->patient->p_name }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">ՀԾՀ</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->social_card }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Անձը հաստատող փաստաթուղթ</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->document_type }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Համարը, ամսաթիվը և հանձման վայրը</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->passport_data }}" class="form-control">--}}
{{--        </div>--}}

{{--    </form>--}}



{{--@endsection--}}

{{--@section('javascript')--}}
{{--    <script src="{{ mix('js/jquery.js') }}"></script>--}}
{{--    <script src="{{ mix('js/datatables.js') }}"></script>--}}
{{--    <script src="{{ mix('js/all.magicsearch.js') }}"></script>--}}

{{--    <script>--}}
{{--        $(document).ready(function() {--}}
{{--            const documentsDt = $("#example").CDataTable({--}}
{{--                rowGroup: {--}}
{{--                    dataSrc: 5--}}
{{--                }--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}
