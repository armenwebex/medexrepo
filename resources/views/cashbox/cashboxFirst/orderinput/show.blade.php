<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{mix('css/print/orderinput.css')}}">
    <title>ԴՐԱՄԱՐԿՂԱՅԻՆ ՄՈՒՏՔԻ ՕՐԴԵՐ</title>
</head>
<body>
<div class="page-wrap">
    <div class="new-page">
        <div class="main-container">
            <br>
            <div class="table-deg">
                <div>
                    <strong class="bottom-line">Ֆանարջյանի անվան ՈՒԱԿ ՓԲԸ</strong>
                </div>
                <br>
                <div class="text-center">
                    <div class="">
                        ԴՐԱՄԱՐԿՂԱՅԻՆ ՄՈՒՏՔԻ ՕՐԴԵՐ N
                            <span class="bottom-line">1</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="bottom-line">2020-10-10</span>
                    </div>
                </div>
                <small class="margin-left3">(Կազմման ամսաթիվ)</small>
                <br><br>
                <table class="table text-center">
                    <tr>
                        <th>Թղթակցող հաշիվե</th>
                        <th>Վերլուծական հաշվառման ծածկագիրը</th>
                        <th>Գումարը</th>
                        <th>նպատակային նաշանակության ծածկագիրը</th>
                    </tr>
                    <tr>
                        <td>50</td>
                        <td>50</td>
                        <td>500000</td>
                        <td>50</td>
                    </tr>
                </table>
                <br>
                <div class="display-flex">
                    <div>Ստացված է</div>
                    <span class="bottom-line">1</span>
                </div>
                <br>
                <div class="bottom-line text-center">Garegin Srvandtyanc</div>
                <div  class="text-center">
                    <small>
                        Կազմակերպության անվանումը
                    </small>
                </div>
                <br>
                <div>Ստացման հիմքը և նպատակը`</div>
                <br>
                <p></p>
                <br>
                <div class="bottom-line text-center">Հինգ հազար դրամ</div>
                <div  class="text-center">
                    <small>
                        Գումարը տառերով
                    </small>
                </div>
                <br>
                <div>Կցվում են`</div>
                <br>
                <div class="bottom-line text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus eligendi maxime perferendis quibusdam sequi. Alias deleniti distinctio dolor enim, eum eveniet exercitationem fugit nam nobis pariatur quae sapiente voluptate voluptates!</div>
                <div  class="text-center">
                    <small>
                        (կցվող փաստաթղթերի թիվը)
                    </small>
                </div>
                <br><br>
                    <div class="display-flex">
                        <div>Վճարող</div>
                        <span class="bottom-line">Grigor Marukhyan</span>
                    </div>
                <br><br>
                    <div class="display-flex">
                        <div>Գանձապահ</div>
                        <span class="bottom-line">Garegin Mkhitaryan</span>
                    </div>
                <br><br>
                <div class="display-flex">
                    <div>Գլխավոր հաշվապահ</div>
                    <span class="bottom-line">Armen Araqelyan</span>
                </div>
            </div>

            <br>
            <div class="table-deg mt-5">
                <div>
                    <strong class="bottom-line">Ֆանարջյանի անվան ՈՒԱԿ ՓԲԸ</strong>
                </div>
                <br>
                <div class="text-center">
                    <div class="">
                        ԴՐԱՄԱՐԿՂԱՅԻՆ ՄՈՒՏՔԻ ՕՐԴԵՐ ԱՆԴՈՐՐԱԳԻՐ N
                        <span class="bottom-line">1</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="bottom-line">2020-10-10</span>
                    </div>
                </div>
                <br>
                <div class="display-flex">
                    <div>Ստացված է</div>
                    <span class="bottom-line">FRVSFEVDSV</span>
                </div>
                <br>
                <div class="bottom-line text-center">Garegin Srvandtyanc</div>
                <div  class="text-center">
                    <small>
                        Կազմակերպության անվանումը
                    </small>
                </div>
                <br>
                <div>Ստացման հիմքը և նպատակը`</div>
                <br>
                <p></p>
                <br>
                <div class="display-flex">
                    <div>Գումարը`(թվերով)</div>
                    <span class="bottom-line">5000000</span>
                    &nbsp;&nbsp;
                    <div>դրամ</div>
                </div>
                <br>
                <div class="bottom-line text-center">Հինգ հազար դրամ</div>
                <div  class="text-center">
                    <small>
                        Գումարը տառերով
                    </small>
                </div>
                <br><br><br>
                    <div class="display-flex">
                        <div>Գլխավոր հաշվապահ</div>
                        <span class="bottom-line">Armen Araqelyan</span>
                    </div>
                <br><br>
                    <div class="display-flex">
                        <div>Գանձապահ</div>
                        <span class="bottom-line">Garegin Mkhitaryan</span>
                    </div>

                <br>

            </div>
        </div>
    </div>
</div>
</body>
</html>

{{--//իսկական--}}
{{--@extends('layouts.cardBase')--}}

{{--@section('css')--}}
{{--    <link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />--}}
{{--    <link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet" />--}}
{{--@endsection--}}

{{--@section('card-header')--}}
{{--    Դրամարկղ--}}
{{--@endsection--}}

{{--@section('card-content')--}}

{{--    <form method="#" action="#">--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Անդորրագիր N</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->id }}" class="form-control">--}}
{{--        </div>--}}

{{--        <div class="form-group">--}}
{{--            <label for="#">Ամսաթիվ</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->created_at }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Գումար (ՀՀ դրամ թվերով)</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->price }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Գումար (ՀՀ դրամ տառերով)</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->sum_text }}" class="form-control">--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="#">Ստացված է</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->patient->f_name }}&nbsp;{{ $order->patient->l_name }}&nbsp;{{ $order->patient->p_name }}" class="form-control">--}}
{{--        </div>--}}


{{--        <div class="form-group">--}}
{{--            <label for="#">Բժիշկ</label>--}}
{{--            <input disabled type="text" name="#" id="#" value="{{ $order->user->f_name }}&nbsp;{{ $order->user->l_name }}&nbsp;{{ $order->user->p_name }}" class="form-control">--}}
{{--        </div>--}}


{{--        <div class="form-group">--}}
{{--            <label for="#">Ստացման հիմք ու նպատակ</label>--}}
{{--            @foreach($order->treatments()->get() as $item)--}}
{{--                <input disabled type="text" name="#" id="#" value="{{ $item->name }}" class="form-control">--}}

{{--            @endforeach--}}
{{--        </div>--}}

{{--    </form>--}}



{{--@endsection--}}

{{--@section('javascript')--}}
{{--    <script src="{{ mix('js/jquery.js') }}"></script>--}}
{{--    <script src="{{ mix('js/datatables.js') }}"></script>--}}
{{--    <script src="{{ mix('js/all.magicsearch.js') }}"></script>--}}

{{--    <script>--}}
{{--        $(document).ready(function() {--}}
{{--            const documentsDt = $("#example").CDataTable({--}}
{{--                rowGroup: {--}}
{{--                    dataSrc: 5--}}
{{--                }--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}
