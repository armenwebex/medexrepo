@extends('layouts.cardBase')

@section('css')
<link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
@endsection

@section('card-header')
<div>
    <h4>Կասսա</h4>
</div>
<div class="card-header-actions">
    <a href="{{ route('cashbox.create') }}" class="btn btn-primary float-right mr-4" type="button">
        <x-svg icon="cui-plus" />
        Նոր կասսա
    </a>
</div>
@endsection

@section('card-content')
    <div class="container">

        <table class="table table-md table-hover table-responsive table-cursor datatable-default" style="width:100%;">
            <thead class="thead-info">
            <tr>
                <th>ID</th>
                <th>Կասսայի անվնաումը</th>
                <th>ip</th>
                <th>ՀՎՀՀ</th>
                <th>Հդմ-ի N</th>
                <th>Գործողություններ</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($lists as $key => $list)
                <tr data-url='{{route("admin.age-lists.edit", $list->id)}}'>
                    <td>{{ $list->id }}</td>
                    <td>{{ $list->cassa_name }}</td>
                    <td>{{ $list->ip }}</td>
                    <td>{{ $list->hvhh }}</td>
                    <td>{{ $list->hdm_number }}</td>
                    <td>
                        <form method="POST" action='{{route("cashbox.destroy",  $list->id)}}' class="d-inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger btn-sm" type="submit">
                                <x-svg icon="cui-trash" />
                            </button>
                        </form>
                        <a href="{{route("cashbox.edit",  $list->id)}}" class="btn btn-primary btn-sm">
                            <x-svg icon="cui-pencil" />
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/datatables.js') }}"></script>
@endsection
