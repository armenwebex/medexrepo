@extends('layouts.cardBase')

@section('css')
<link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
@endsection

@section('card-header')
<div>
    <h4>Կասսա</h4>
</div>
<div class="card-header-actions">
    <a href="{{ route('cashbox.index') }}" class="btn btn-primary float-right mr-4" type="button">
        <x-svg icon="cui-plus" />
        Կասսաներ
    </a>
</div>
@endsection

@section('card-content')
    <div class="container">


    </div>
    <form action="{{ route('cashbox.store') }}" method="POST">
        @csrf
        <li class="list-group-item">
            <label><strong>Կասսայի անվանում</strong></label>
            <select class="form-control mb-2" name="cassa_name">
                <option value="Դրամարկղ 1">Դրամարկղ 1</option>
                <option value="Դրամարկղ 2">Դրամարկղ 2</option>
                <option value="Դրամարկղ 3">Դրամարկղ 3</option>
                <option value="Դրամարկղ 4">Դրամարկղ 4</option>
            </select>
            <label><strong>ՀՎՀՀ</strong></label>
            <input class="form-control magic-search ajax my-2" name="hvhh" >
            <label><strong>IP</strong></label>
            <input class="form-control magic-search ajax my-2" name="ip">
            <label><strong>Հդմ-ի N</strong></label>
            <input class="form-control magic-search ajax my-2" name="hdm_number" >
        </li>
        <button type="submit" class="btn btn-primary">Ավելացնել</button>
    </form>
@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/datatables.js') }}"></script>
@endsection
