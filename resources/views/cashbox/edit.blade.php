@extends('layouts.cardBase')

@section('css')
<link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
@endsection

@section('card-header')
<div>
    <h4>Կասսա</h4>
</div>
<div class="card-header-actions">
    <a href="{{ route('cashbox.index') }}" class="btn btn-primary float-right mr-4" type="button">
        <x-svg icon="cui-plus" />
        Կասսաներ
    </a>
</div>
@endsection

@section('card-content')
    <div class="container">


    </div>
    <form action="{{ route('cashbox.update', [$item->id]) }}" method="POST">
        @method('put')
        @csrf
        <li class="list-group-item">
            <label><strong>Կասսայի անվանում</strong></label>
            <select class="form-control mb-2" name="cassa_name">
                <option @if($item->cassa_name=='Դրամարկղ 1') selected @endif value="Դրամարկղ 1">Դրամարկղ 1</option>
                <option @if($item->cassa_name=='Դրամարկղ 2') selected @endif value="Դրամարկղ 2">Դրամարկղ 2</option>
                <option @if($item->cassa_name=='Դրամարկղ 3') selected @endif value="Դրամարկղ 3">Դրամարկղ 3</option>
                <option @if($item->cassa_name=='Դրամարկղ 4') selected @endif value="Դրամարկղ 4">Դրամարկղ 4</option>
            </select>
            <label><strong>ՀՎՀՀ</strong></label>
            <input class="form-control magic-search ajax my-2" name="hvhh" value="{{ $item->hvhh }}">
            <label><strong>IP</strong></label>
            <input class="form-control magic-search ajax my-2" name="ip" value="{{ $item->ip }}">
            <label><strong>Հդմ-ի N</strong></label>
            <input class="form-control magic-search ajax my-2" name="hdm_number" value="{{ $item->hdm_number }}">
        </li>
        <br>
        <button type="submit" class="btn btn-primary">Փոփոխել</button>
    </form>
@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/datatables.js') }}"></script>
@endsection
