@extends('layouts.cardBase')

@section('css')
    <link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet" />
@endsection

@section('card-header')
    Ավելացնել Դրամարկղ
@endsection

@section('card-content')
    <div class="container">
        <form class="ajax-submitable" action="{{ route('add_cashbox') }}" method="POST">
            @csrf
            <li class="list-group-item">
                <label><strong>Կասսայի անվանում</strong></label>
                <select class="form-control mb-2" name="cassa_name">
                    <option value="Դրամարկղ 1">Դրամարկղ 1</option>
                    <option value="Դրամարկղ 2">Դրամարկղ 2</option>
                    <option value="Դրամարկղ 3">Դրամարկղ 3</option>
                    <option value="Դրամարկղ 4">Դրամարկղ 4</option>
                </select>
                <label><strong>ՀՎՀՀ</strong></label>
                <input class="form-control magic-search ajax my-2" name="hvhh" >
                <label><strong>IP</strong></label>
                <input class="form-control magic-search ajax my-2" name="ip">
                <label><strong>Հդմ-ի N</strong></label>
                <input class="form-control magic-search ajax my-2" name="hdm_number" >
            </li>
            @include('shared.forms.list_group_item_submit', ['btn_text'=>'Ավելացնել'])
        </form>
    </div>

@endsection

