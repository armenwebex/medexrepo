<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ԷՍԳ</title>
    <link rel="stylesheet" href="{{mix('css/print/esg.css')}}">

    @isset ($for_pdf)
        <style>
            *{ font-family: DejaVu Sans !important;}
        </style>
    @endif

</head>
<body>

<?php

use App\Enums\StationaryDiagnosisEnum;
use App\Enums\StationaryMedicineSideEffectEnum as MedSideEffect;
use App\Enums\StationarySurgeryEnum;

use App\Enums\Samples\SampleDiagnosesEnum;
use App\Enums\Samples\SampleTreatmentsEnum;

?>
<div class="page-wrap">
    <br><br>
    <div class="main-container">
        <div class="text-center">ՀՀ ԱՌՈՂՋԱՊԱՀՈՒԹՅԱՆ ՆԱԽԱՐԱՐՈՒԹՅՈՒՆ</div>
        <div class="text-center"><<Վ․Ա․ ՖԱՆԱՐՋՅԱՆԻ ԱՆՎ․ ՈՒՌՈՒՑՔԱԲԱՆՈՒԹՅԱՆ</div>
        <div class="text-center">ԱԶԳԱՅԻՆ ԿԵՆՏՐՈՆ>>ՓԲԸ</div>
        <br><br>
        <div class="display-flex justifiy-content-center">
            <div>ԷԼԵԿՏՐԱՍՐՏԱԳՐՈՒԹՅՈՒՆ N՝</div>
            <span class="bottom-line">{{$em->electric}}</span>
        </div>
        <br>
        <div class="display-flex justifiy-content-center">
            <div>ՀԻՎԱՆԴՈՒԹՅԱՆ ՊԱՏՄԱԳԻՐ N՝</div>
            <span class="bottom-line">{{$em->history}}</span>
        </div>
        <br>
        <div class="display-flex justifiy-content-center">
            <div>ԱՄԲՈՒԼԱՏՈՐ ՔԱՐՏ N՝</div>
            <span class="bottom-line">{{$em->ambulator}}</span>
        </div>
        <br>
        <div class="display-flex justifiy-content-center">
            <div>Ամսաթիվ՝</div>
            <span class="bottom-line">{{$em->date}}</span>
        </div>
        <br>
        <div class="text-center">
                <h3>ԱՏԱՄԻԿՆԵՐԻ և ԻՆՏԵՐՎԱԼՆԵՐԻ ՉԱՓՈՒՄԸ</h3>
        </div>
        <div class="text-center">
            <h3>ИЗМЕРЕНИЕ ЗУБЦОВ И ИНТЕРВАЛОВ</h3>
        </div>
        <br>
        <div class="display-flex">
            <div>PQ՝</div>
            <span class="bottom-line">{{$em->pq}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>QRS՝</div>
            <span class="bottom-line">{{$em->qrs}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>QRST՝</div>
            <span class="bottom-line">{{$em->qrst}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>RR՝</div>
            <span class="bottom-line">{{$em->rr}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>ՌԻԹՄ ՐՈՊԵՈՒՄ՝</div>
            <span class="bottom-line">{{$em->rhythm}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>Սիսթոլիկ ցուցանիշ՝</div>
            <span class="bottom-line">{{$em->sistolik}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>Սիսթոլիկ ցուց․ նորմալ տվյալ ռիթմի համար՝</div>
            <span class="bottom-line">{{$em->sistolikritm}}</span>
        </div>
        <br>
        <div class="text-center">
            <h3>ՆԿԱՐԱԳՐՈՒԹՅՈՒՆ</h3>
        </div>
        <div class="text-center">
            <h3>ОПИСАНИЕ</h3>
        </div>
        <br>
        <div class="display-flex">
            <div>P՝</div>
            <span class="bottom-line">{{$em->p}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>R՝</div>
            <span class="bottom-line">{{$em->r}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>Q՝</div>
            <span class="bottom-line">{{$em->q}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>S՝</div>
            <span class="bottom-line">{{$em->s}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>ST՝</div>
            <span class="bottom-line">{{$em->st}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>T՝</div>
            <span class="bottom-line">{{$em->t}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>ՎՈԼՏԱԺ՝</div>
            <span class="bottom-line">{{$em->voltage}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>III հավելումը ներշնչման ժամանակ՝</div>
            <span class="bottom-line">{{$em->inspiration}}</span>
        </div>
        <br>
        <div class="text-center">
            <h3>ԵԶՐԱԿԱՑՈՒԹՅՈՒՆ</h3>
        </div>
        <div class="text-center">
            <h3>ЗАКЛЮЧЕНИЕ</h3>
        </div>
        <br>
        <div class="display-flex">
            <div>Ծանոթագրություն՝</div>
            <span class="bottom-line">{{$em->note}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>Վիրահատող բժիշկ՝</div>
            <span class="bottom-line">{{ $em->attending_doctor->full_name}}</span>
        </div>

    </div>
</body>
</html>
