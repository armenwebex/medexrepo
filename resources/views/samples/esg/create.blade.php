@extends('layouts.cardBase')

@section('css')
    <link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet"/>
@endsection


@section('card-header')
@section('card-header-classes', '')


<div class="text-center">
    <h3>ԷՍԳ</h3>
</div>
@endsection


@section('card-content')
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <form action="{{route('samples.patients.esg.store' , ['patient'=> $patient])}}"  method="POST">
                    @csrf
                    <div class="container">

                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>ԷԼԵԿՏՐԱՍՐՏԱԳՐՈՒԹՅՈՒՆ N՝</strong>
                                    </div>

                                    <div class="col-md-8">
                                        <input name="electric" min="0" type="number" class="form-control col-md-3">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>ՀԻՎԱՆԴՈՒԹՅԱՆ ՊԱՏՄԱԳԻՐ N՝</strong>
                                    </div>

                                    <div class="col-md-8">
                                        <input min="0" name="history" type="number" class="form-control col-md-3">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>ԱՄԲՈՒԼԱՏՈՐ ՔԱՐՏ N՝</strong>
                                    </div>

                                    <div class="col-md-8">
                                        <input name="ambulator" min="0" type="number" class="form-control col-md-3">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>Ամսաթիվ՝</strong>
                                    </div>

                                    <div class="col-md-8">
                                        <x-forms.text-field name="date" type="date" value="" validationType="ajax" label=""/>
                                        <em class="error text-danger" data-input="date"></em>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item text-center">
                                <h3>ԱՏԱՄԻԿՆԵՐԻ և ԻՆՏԵՐՎԱԼՆԵՐԻ ՉԱՓՈՒՄԸ</h3>
                                <h3>ИЗМЕРЕНИЕ ЗУБЦОВ И ИНТЕРВАЛОВ</h3>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>PQ՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea  name="pq" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>QRS՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea  name="qrs" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>QRST՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="qrst" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>RR՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="rr" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>ՌԻԹՄ ՐՈՊԵՈՒՄ՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="rhythm" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>Սիսթոլիկ ցուցանիշ՝</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <input name="sistolik" type="text" class="form-control" />
                                    </div>
                                    <div class="">
                                        %
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-6">
                                        <strong>Սիսթոլիկ ցուց․ նորմալ տվյալ ռիթմի համար՝</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <input name="sistolikritm" type="text" class="form-control" />
                                    </div>

                                </div>
                            </li>
                            <li class="list-group-item text-center">
                                <h3>ՆԿԱՐԱԳՐՈՒԹՅՈՒՆ</h3>
                                <h3>ОПИСАНИЕ</h3>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>P՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="p" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>R՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="r" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>Q՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="q" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>S՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="s" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>ST՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="st" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>T՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="t" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>ՎՈԼՏԱԺ՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="voltage" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>III հավելումը ներշնչման ժամանակ՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="inspiration" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item text-center">
                                <h3>ԵԶՐԱԿԱՑՈՒԹՅՈՒՆ</h3>
                                <h3>ЗАКЛЮЧЕНИЕ</h3>
                            </li>
                            <li class="list-group-item">
                                <div class="form-row align-items-center text-center">
                                    <div class="col-md-4">
                                        <strong>Ծանոթագրություն՝</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="note" class="form-control col-md-12"></textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <strong>Վիրահատող բժիշկ՝</strong>
                                <x-forms.magic-search hidden-id="ape_attending_doctor_id" validationType="ajax"
                                                      hidden-name="attending_doctor_id"
                                                      placeholder="Ընտրել բուժող բժիշկին․․․" class="magic-search ajax my-2"
                                                      data-list-name="users"
                                                      value=""/>
                            </li>
                        </ul>
                        @include('shared.forms.list_group_item_submit', ['btn_text'=>'Ուղարկել'])
                    </div>

                </form>
            </div>
            <div class="col-md-1"></div>
        </div>

    </div>
@endsection


@section('javascript')
    <script src="{{ mix('js/jquery.js') }}"></script>
    <script src="{{ mix('js/all.magicsearch.js') }}"></script>
    <script src="{{ mix('/js/components/Select.js') }}"></script>
@endsection

<script>
    const usersUrl = @json(route('lists.users_full'));
    $('.user_search').magicsearch(
        window.medexMagicSearch.assignConfigs({
            type: "ajax",
            dataSource: `${usersUrl}?groupByRole=doctor`,
            fields: ["f_name", "l_name"],
            id: "id",
            format: "%f_name% %l_name%",
            success: function ($input, data) {
                console.log(data)
                const hidden_input_id = $input.data('hidden');
                $(hidden_input_id).val($input.attr("data-id"));
            },
            afterDelete: function ($input, data) {
                const hidden_input_id = $input.data("hidden");
                $(hidden_input_id).val("");
            }
        })
    );

</script>
