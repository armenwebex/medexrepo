@extends('layouts.cardBase')

@section('css')
<link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
@endsection

@section('card-header')
<div>
    <h4>ԷՍԳ</h4>
    <h5>{{ $patient->all_names }}</h5>
</div>
<div class="card-header-actions">
    <a href="{{ route("samples.patients.ape.create", ['patient' => $patient]) }}" class="btn btn-primary float-right mr-4" type="button">
        <x-svg icon="cui-plus" />
        Նոր հետազոտություն
    </a>
</div>
@endsection

@section('card-content')

<table class="table table-striped table-bordered table-cursor datatable-default">
    <thead>
        <tr>
            <th>ID</th>
            <th>Ամսաթիվ</th>
            <th>Բուժող բժիշկ</th>
            <th>Կարգավիճակ</th>
            <th>Գործողություններ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($esgs as $esg)
        <tr data-url="{{ route("samples.patients.esg.show", ['patient' => $patient , $esg->id]) }}">
            <td>
                {{ $esg->id }}
            </td>
            <td>
                {{ $esg->date }}
            </td>
            <td>
                {{ $esg->attending_doctor->full_name}}
            </td>

            <td>
                {{ optional($esg->approvement)->status  ? "Հաստատված" : "Սպասման մեջ"}}
                {{ $esg->approvementStatus() }}
            </td>
            <td>
                <a href="{{ route('samples.patients.esg.show', ['patient' => $patient , $esg->id]) }}" class="btn btn-info btn-sm">
                    <x-svg icon="cui-external-link" />
                </a>

                @can('belongs-to-user', $esg)
                <a href="{{ route('samples.patients.esg.edit', ['patient' => $patient , $esg->id]) }}" class="btn btn-primary btn-sm">
                    <x-svg icon="cui-pencil" />
                </a>
                @endcan
                @if($esg->approvement)
                @can('user-can-approve', $esg)
                <form method="POST" action="{{ route("approvements.update", ["approvement" => $apse->approvement]) }}" class="d-inline">
                    @csrf
                    @method("PATCH")
                    <button class="btn btn-success btn-sm" {{ $esg->approvement->status ? "disabled" : "" }}>
                        <x-svg icon="cui-check" />
                    </button>
                </form>
                @endcan
                @endif
                @can('belongs-to-user', $esg)
                <form method="POST" action="{{route('samples.patients.esg.destroy', [$patient->id , $esg->id]) }}" class="d-inline">
                    @csrf
                    @method('delete')

                    <button class="btn btn-danger btn-sm"  >
                        <x-svg icon="cui-trash" />
                    </button>
                </form>
                @endcan
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/datatables.js') }}"></script>
@endsection
