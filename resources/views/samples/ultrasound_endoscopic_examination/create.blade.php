@extends('layouts.cardBase')

@section('css')
<link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet" />
@endsection


@section('card-header')
@section('card-header-classes', '')

<div class="text-center">
    <h3>Էնդոսկոպիկ ուլտրաձայնային հետազոտություն</h3>
</div>
@endsection


@section('card-content')

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <form class="ajax-submitable" action="{{route('samples.patients.uex.store', ['patient'=> $patient])}}" method="POST">
                @csrf
                <ul class="list-group">
                    <li class="list-group-item">
                        <label><b>Հետազոտության տեսակ</b></label>
                        <span style="color: red">*</span>
                        <x-forms.text-field type="textarea" name="research_type"  validation-type="ajax" placeholder="լրացման ազատ դաշտ․․․" class="mt-2"
                                            value=""  />

                    <li class="list-group-item">
                        <strong>
                            <span class="badge badge-light mr-1">1.</span>
                            Ազգանուն, անուն, հայրանուն
                        </strong>
                        <ins class="ml-4">{{$patient->all_names}}</ins>
                    </li>
                    <!-- 2 -->
                    <li class="list-group-item">
                        <strong>
                            <span class="badge badge-light mr-1">2.</span>
                            Ծննդյան թիվը՝
                        </strong>
                        <ins class="ml-4">{{$patient->birth_date}}</ins>

                        <hr>

                        <div>
                            <label><b>Նկարագիր</b></label>
                            <span style="color: red">*</span>
                            <x-forms.text-field type="textarea" name="description_comment" validation-type="ajax" placeholder="լրացման ազատ դաշտ․․․"
                                                value="" />
                        </div>
                        <div class="mt-4">
                            <label><b>Եզարակացություն</b></label>
                            <span style="color: red">*</span>
                            <x-forms.text-field type="textarea" name="conclusion_comment" validation-type="ajax" placeholder="լրացման ազատ դաշտ․․․"
                                                value="" />
                        </div>

                        <div class="mt-4">
                            <label><b>Խորհուրդ է տրվում</b></label>
                            <span style="color: red">*</span>
                            <x-forms.text-field type="textarea" name="recommended_comment" validation-type="ajax" placeholder="լրացման ազատ դաշտ․․․"
                                                value="" />
                        </div>

                    </li>

                    <li class="list-group-item">
                        <div class="form-row align-items-center">
                            <div class="col-md-4">
                                <strong>Ընդունման ամսաթիվը և ժամը՝</strong>
                            </div>
                            <div class="col-md-8">
                                <x-forms.text-field name="date" validation-type="ajax" type="datetime-local"
                                                    value="" label="" />
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item">
                        {{-- <div class="col-md-12"> --}}
                        <strong>Բուժող բժիշկ՝</strong>
                        <x-forms.magic-search hidden-id="ue_attending_doctor_id" hidden-name="attending_doctor_id"
                                              placeholder="Ընտրել բուժող բժիշկին․․․" class="magic-search ajax my-2" data-list-name="users"
                                              value="" />
                        {{-- </div> --}}
                    </li>


                    @include('shared.forms.list_group_item_submit', ['btn_text'=>'Ուղարկել'])
                    </li>
                </ul>

            </form>
        </div>
        <div class="col-md-1"></div>
    </div>

</div>
@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/all.magicsearch.js') }}"></script>

@endsection
