@extends('layouts.cardBase')

@section('css')
    <link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet"/>
@endsection

@php
    use App\Enums\StationarySurgeryEnum;
    use App\Enums\StationaryDiagnosisEnum;
@endphp

@section('card-header')
@section('card-header-classes', '')
    <div class="text-center">
        <h3>Քաղցկեղով {{__('patient.patient')}} հսկիչ քարտ</h3>
    </div>
@endsection

@section('card-content')
<div class="container">
<form action="{{ route('samples.patients.cpcc.store', $patient->id)}}" class="ajax-submitable -off" method="POST">
    @csrf

    <ul class="list-group">

        <li class="list-group-item">
            <h5 class="text-primary">Պացիենտի կցագրում</h5>
            <div class="form-row">
                <div class="col-md-6">
                    <x-forms.magic-search
                    hidden-name="attached_clinic_id" hidden-id="attached_clinic_id"
                    id="attached_clinic_id_search" class="my-2"
                    placeholder="Ընտրել բուժ․ հաստատության անվանումը․․․"
                    value='{{old("attached_clinic_id")}}'/>
                    <em class="error text-danger" data-input="attached_clinic_id"></em>
                </div>
                <div class="col-md-6">
                        <x-forms.text-field type="text" class="my-2 font-weight-bold" id="region_name" name="region_name"
                        value='{{old("region_name")}}' disabled/>
                        {{-- <x-forms.text-field type="hidden" id="region_id" name="region_id"
                        value='{{old("region_id")}}' disabled/> --}}
                </div>
            </div>
        </li>



        <!-- 1 -->
        <li class="list-group-item">
            <div class="form-row justify-content-between">
                <div class="col-md-6">
                    <strong>
                        <span class="badge badge-light mr-1">1.</span>
                        Ազգանուն, անուն, հայրանուն
                    </strong>
                    <ins class="ml-4">{{$patient->all_names ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">2.</span>
                        Սեռը՝
                    </strong>
                    <ins class="ml-4">{{$patient->sex ?? ''}}</ins>
                </div>
            </div>
        </li>

        <!-- 2 -->
        <li class="list-group-item">
            <div class="form-row align-items-center">
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">3.</span>
                        Ծննդյան ամսաթիվ՝
                    </strong>
                    <ins class="ml-4">{{$patient->birth_date_reversed ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">4.</span>
                        ՀԾՀ՝
                    </strong>
                    <ins class="ml-4">{{$patient->soc_card ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">5.</span>
                        Անձնագիր՝
                    </strong>
                    <ins class="ml-4">{{$patient->passport ?? ''}}</ins>
                </div>
            </div>
        </li>


        <!-- 3 -->
        <li class="list-group-item">
            <div class="form-row justify-content-between">
                <div class="col-md-6">
                    <strong>
                        <span class="badge badge-light mr-1">6.</span>
                        Քաղաքացիություն՝
                    </strong>
                    <ins class="ml-4">{{$patient->citizenship ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">7.</span>
                        Ազգություն՝
                    </strong>
                    <ins class="ml-4">{{$patient->nationality ?? ''}}</ins>
                </div>
            </div>
        </li>

        <!-- 4 -->
        <li class="list-group-item">
            <div class="form-row justify-content-between">
                <div class="col-md-6">
                    <strong>
                        <span class="badge badge-light mr-1">8.</span>
                        Սոցիալ կենցաղային պայմաններ՝
                    </strong>
                    <ins class="ml-4">{{$patient->social_living_condition->name ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">9.</span>
                        Ամուսնական կարգավիճակ՝
                    </strong>
                    <ins class="ml-4">{{$patient->marital_status->name ?? ''}}</ins>
                </div>
            </div>
        </li>

        <!-- 5 -->
        <li class="list-group-item">
            <div class="form-row justify-content-between">
                <div class="col-md-8">
                    <strong>
                        <span class="badge badge-light mr-1">10.</span>
                        Աշխատանքային առանձնահատկություններ՝
                    </strong>
                    <ins class="ml-4">{{$patient->working_feature->name ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">11.</span>
                        Կրթություն՝
                    </strong>
                    <ins class="ml-4">{{$patient->education->name ?? ''}}</ins>
                </div>
            </div>
        </li>

        <li class="list-group-item">
            <div class="form-row justify-content-between">
                <div class="col-md-6">
                    <strong>
                        <span class="badge badge-light mr-1">12.</span>
                        էլ․ փոստ՝
                    </strong>
                    <ins class="ml-4">{{$patient->email ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">13.</span>
                        Տարիք՝
                    </strong>
                    <ins class="ml-4">{{$patient->hasAge() ? $patient->age :''}}</ins>
                </div>
            </div>
        </li>



        <!-- 6 -->
        <li class="list-group-item">
            <div class="form-row">
                <div class="col-md-12">
                    {{-- @php
                        dump($patient->age_and_type);
                        dd($patient->age_range);
                        dump($patient->age_code);
                        dump($patient->hasAge());
                    @endphp --}}
                    {{-- @dump($patient->age_code) --}}
                </div>
            </div>
            <div class="form-row justify-content-between text-primary">
                <div class="col-md-6">
                    <strong>
                        <span class="badge badge-light mr-1">14.</span>
                        Տարիքը հայտնաբերման տարում՝
                    </strong>
                    <ins class="ml-4">{{$patient->hasAge() ? $patient->age : ''}}</ins>

                    {{-- <input type="hidden" name="patient_id" value="{{$patient->id}}"> --}}
                    <input type="hidden" name="age_at_discovery_year" value="{{$patient->ageAndType['age']}}">
                    <input type="hidden" name="age_type_at_discovery_year" value="{{$patient->ageAndType['type']}}">
                    <input type="hidden" name="age_code_at_discovery_year" value="{{$patient->hasAgeCode() ? $patient->age_code : ''}}">
                    {{-- <input type="hidden" name="age_list_id" value="{{$patient->hasAgeCode() ? $patient->age_list_id : ''}}"> --}}
                </div>
                <div class="col-md-6">
                    <strong>
                        <span class="badge badge-light mr-1">15.</span>
                        Տարիքային կոդ հայտնաբերման տարում՝
                    </strong>
                    <ins class="ml-4">{{$patient->hasAgeCode() ? $patient->age_code : ''}}</ins>
                </div>
            </div>
        </li>
        <hr/>

        <li class="list-group-item">
            <div class="form-row justify-content-between">
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">16.</span>
                        Քաղաքային հեռ․՝
                    </strong>
                    <ins class="ml-4">{{$patient->c_phone ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">17.</span>
                        Բջջային հեռ․՝
                    </strong>
                    <ins class="ml-4">{{$patient->m_phone ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">18.</span>
                        Բնակության վայր՝
                    </strong>
                    <ins class="ml-4">{{$patient->living_place->name ?? ''}}</ins>
                </div>
            </div>
        </li>

        <!-- Գրանցման Հասցեն կար և նախկինում -->
        <li class="list-group-item">
            <h5 class="mb-4">Գրանցման Հասցե</h5>
            <div class="form-row justify-content-between">
                <div class="col-md-3">
                    <strong>
                        <span class="badge badge-light mr-1">19.</span>
                        Մարզ՝
                    </strong>
                    <ins class="ml-4">{{$patient->residence_region ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">20.</span>
                        Քաղաք/Գյուղ՝
                    </strong>
                    <ins class="ml-4">{{$patient->town_village ?? ''}}</ins>
                </div>
                <div class="col-md-5">
                    <strong>
                        <span class="badge badge-light mr-1">21.</span>
                        Փողոց/Շենք՝
                    </strong>
                    <ins class="ml-4">{{$patient->street_house ?? ''}}</ins>
                </div>
            </div>
        </li>

        <!-- Բնակության Հասցեն կար և նախկինում -->
        <li class="list-group-item">
            <h5 class="mb-4">Բնակության Հասցե</h5>
            <div class="form-row justify-content-between">
                <div class="col-md-3">
                    <strong>
                        <span class="badge badge-light mr-1">22.</span>
                        Մարզ՝
                    </strong>
                    <ins class="ml-4">{{$patient->residence_region_residence ?? ''}}</ins>
                </div>
                <div class="col-md-4">
                    <strong>
                        <span class="badge badge-light mr-1">23.</span>
                        Քաղաք/Գյուղ՝
                    </strong>
                    <ins class="ml-4">{{$patient->town_village_residence ?? ''}}</ins>
                </div>
                <div class="col-md-5">
                    <strong>
                        <span class="badge badge-light mr-1">24.</span>
                        Փողոց/Շենք՝
                    </strong>
                    <ins class="ml-4">{{$patient->street_house_residence ?? ''}}</ins>
                </div>
            </div>
        </li>


        <!-- stationary_social_packages -->
        <li class="list-group-item">
            <h5>Սոց․ խումբ</h5>
            @forelse ($stationaries as $stationary)
                {{-- @dump($stationary->number .' -- '. $stationary->admission_date) --}}
                <strong>Ստացիոնար {{__('patient.patient')}} բժշկական քարտ №{{$stationary->number}}</strong>

                @forelse ($stationary->stationary_social_packages as $stationary_social_package)
                <div class="row justify-content-end">
                    {{-- @dump( $stationary_social_package->package_item->name) --}}
                    <div class="col-3">{{optional($stationary_social_package->package_item)->created_at ?? '--'}}</div>
                    <div class="col-8">{{optional($stationary_social_package->package_item)->name ?? '--'}}</div>
                </div>
                @empty
                @endforelse
            @empty
            @endforelse
        </li>
        <hr class="my-2">

        <li class="list-group-item text-primary">
            <div class="row">
                <div class="col-md-7">
                    <strong>
                        Առաջնակի բազմակի տեղակայման չարորակ նորագոյացություն՝
                    </strong>
                </div>
                <div class="col-md-5">
                    <x-forms.checkbox-radio pos="align" id="pmmnd-radio1" value="1" {{--old-default="{{$stationary->is_urgent}}"--}}
                    class="pmmnd-radio" name="primary_multiple_malignant_neoplasm_deployment" label="Այո" />
                    <x-forms.checkbox-radio pos="align" id="pmmnd-radio2" value="0" {{--old-default="{{$stationary->is_urgent}}"--}}
                    class="pmmnd-radio" name="primary_multiple_malignant_neoplasm_deployment" label="Ոչ" />
                </div>
                <div class="col-md-12 text-center my-1">
                    <em class="error text-danger" data-input="primary_multiple_malignant_neoplasm_deployment"></em>
                </div>
            </div>
        </li>

        <li class="list-group-item text-primary">
            <div class="row synchronous-radio-group">
                <div class="col-md-5">
                    <strong>
                        Միաժամանակյա (Սինքրոն)՝
                    </strong>
                    <x-forms.checkbox-radio pos="align" id="synchronous-radio1" value="1" {{--old-default="{{$stationary->is_urgent}}"--}}
                        name="synchronous" label="" />
                </div>
                <div class="col-md-5">
                    <strong>
                        Ոչ միաժամանակյա (Մետաքրոն)՝
                    </strong>
                    <x-forms.checkbox-radio pos="align" id="synchronous-radio2" value="0" {{--old-default="{{$stationary->is_urgent}}"--}}
                    name="synchronous" label="" />
                </div>
                <div class="col-md-12 text-center my-1">
                    <em class="error text-danger" data-input="synchronous"></em>
                </div>
            </div>
        </li>

        <h4 class="text-center my-4">Հետազոտություններ, Հայտնաբերման եղանակներ</h4>

        <li class="list-group-item text-primary">
            <div class="form-row">
                <div class="col-md-3">
                    <strong>
                        Դիմելու նպատակը՝
                    </strong>
                    <x-forms.magic-search class="magic-search ajax" data-catalog-name="application_purpose_list"
                        hidden-id="application_purpose_id" hidden-name="application_purpose_id" placeholder="Ընտրել դիմելու նպատակը․․․"
                        {{-- value='{{old("application_purpose_id", $patient->working_feature_id)}}' --}}
                        />
                        <em class="error text-danger" data-input="application_purpose_id"></em>
                </div>
                <div class="col-md-3">
                    <strong>
                        Հաշվառում ՈՒԱԿ-ում՝
                    </strong>
                    <x-forms.text-field type="date" name="cpcc_registration_date" validationType="ajax"
                    value='{{old("cpcc_registration_date")}}'/>
                </div>
                <div class="col-md-6">
                    <strong>
                        Հաշվառման վերցնելու տարբերակները՝
                    </strong>
                    <x-forms.magic-search class="magic-search ajax" data-catalog-name="registration_option_list"
                        hidden-id="registration_option_id" hidden-name="registration_option_id" placeholder="Ընտրել հաշվառման վերցնելու տարբերակները․․․"
                        {{-- value='{{old("application_purpose_id")}}' --}}
                        />
                        <em class="error text-danger" data-input="registration_option_id"></em>
                </div>
            </div>
        </li>

        <li class="list-group-item">
            @forelse ($first_info as $info_item)
            {{-- @dump($info_item) --}}
            <div class="form-row">
                <div class="col-md-6">
                    <strong> Մինչ ախտորոշման հաստատվելը, որտեղ է հետազոտվել՝ </strong>
                    <div class="my-2">{{$info_item->first_clinic_item->name  ?? ''}}</div>
                </div>
                <div class="col-md-6">
                    <strong> Մինչ ախտորոշման հաստատվելը, երբ է հետազոտվել՝ </strong>
                    <div class="my-2">{{$info_item->first_clinic_date ?? ''}}</div>
                </div>
            </div>
            @empty
            @endforelse
        </li>

        <li class="list-group-item">
            @forelse ($first_info as $info_item)
            <div class="form-row">
                <div class="col-md-6">
                    <strong> Որտեղ է առաջին անգամ ախտորոշվել՝ </strong>
                    <div class="my-2">{{$info_item->first_discovered_item->name  ?? ''}}</div>
                </div>
                <div class="col-md-6">
                    <strong> Երբ է առաջին անգամ ախտորոշվել՝ </strong>
                    <div class="my-2">{{$info_item->first_discovered_date ?? ''}}</div>
                </div>
            </div>
            @empty
            @endforelse
        </li>

        <h4  class="text-center my-4">Ախտորոշում</h4>
        <li class="list-group-item">
            <h5>Լոկալիզացիա (Ստացիոնար՝ Հիմնական հիվանդության ախտորոշումը՝ գրվում է դուրս գրումից հետո)</h5>
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար {{__('patient.patient')}} բժշկական քարտ №{{$stationary->number}}</strong>
                @php
                    $primary_diagnosis = $stationary->stationary_primary_diagnosis();

                    $primary_disease_code = optional($primary_diagnosis)->disease_item->code ?? '--';
                    $primary_disease_name = optional($primary_diagnosis)->disease_item->name ?? '--';
                    $primary_diagnosis_created_at = optional($primary_diagnosis)->created_at ?? '--';
                    $primary_diagnosis_comment = optional($primary_diagnosis)->diagnosis_comment ?? '--';
                    // dump($primary_diagnosis);
                @endphp
                <div class="row justify-content-end">
                    <div class="col-3">
                        <div class="rounded-sm  border-bottom p-2">{{$primary_diagnosis_created_at}}</div>
                    </div>
                    <div class="col-8">
                        <div class="rounded-sm  border-bottom p-2">{{$primary_disease_code .' - '.$primary_disease_name}}</div>
                    </div>
                    <div class="col-8">
                        <div class="rounded-sm  border-bottom p-2">{{$primary_diagnosis_comment}}</div>
                    </div>
                </div>
            @empty
            @endforelse
        </li>

        <!-- ավելացնել ու բերել  histological_examinations -->
        <!-- ավելացնել "Հյուսվածքաբանական (բջջաբանական) հետազոտություն" ձևանմուշում DONE -->
        <li class="list-group-item">
            <h5>Հյուսվածքաբանական կառուցվածք</h5>
            @forelse ($histological_examinations as $histological_examination)
                <strong>Հյուսվածքաբանական (բջջաբանական) հետազոտություն №{{$histological_examination->id}}</strong>
                @php
                    $histological_structure_lists = $histological_examination->histological_structure_lists;
                @endphp
                @forelse ($histological_structure_lists as $hsl_key => $histological_structure_list)
                    <div class="row justify-content-end">
                        <div class="col-2">
                            <div class="rounded-sm  border-bottom p-2">{{$hsl_key+1}}</div>
                        </div>
                        <div class="col-8">
                            <div class="rounded-sm  border-bottom p-2">
                                {{$histological_structure_list->code}} - {{$histological_structure_list->name}}
                            </div>
                        </div>
                    </div>
                @empty
                @endforelse

            @empty
            @endforelse
        </li>

        <li class="list-group-item text-primary">
            <div class="form-row">
                <div class="col-md-12">
                    <strong> Ինչ հետազոտությամբ է հաստատվել ախտորոշումը </strong>
                    <x-forms.magic-search class="magic-search ajax" data-catalog-name="researches_lists"
                        hidden-id="research_list_ids" hidden-name="research_list_ids" placeholder="Ընտրել ախտորոշման հաստատման հետազոտությունները․․․"
                        {{-- value='{{old("application_purpose_id", $patient->working_feature_id)}}' --}}
                        data-multiple='true'/>
                        <em class="error text-danger" data-input="research_list_ids"></em>
                </div>
            </div>
        </li>

        <!-- վեջացնել, դառնալ սրան -->
        <li class="list-group-item">
            <h5>Օնկոմարկեր</h5>
                @forelse ($immunologicalExaminationPatternN7 as $immunologicalPattern)
                    <strong class="px-4">Իմունաբանական լաբորատորիա բժշկական ձեվ n 7</strong>
                    <a href="{{ route("samples.patients.iep-n7.show", ["patient" => $patient->id, "iep_n7"=> $immunologicalPattern->id]) }}"
                        class="btn btn-info btn-sm" target="_blank">
                        <x-svg icon="cui-external-link" />
                    </a>

                    <div class="row justify-content-end">
                        <div class="col-7">Կենսանյութը վերցնելու ամսաթիվ</div>
                        <div class="col-4">{{optional($immunologicalPattern)->date ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">Հետազոտության պատասխանի տրման ամսաթիվ</div>
                        <div class="col-4">{{optional($immunologicalPattern)->date_research ?? '--'}}</div>
                    </div>
                    <hr class="w-75 hr-dashed">

                    <!--  բուն օնկոմարկերները -->
                    <div class="row justify-content-end">
                        <div class="col-7">AFP / ալֆա ֆետոպրոտեին/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->AFP ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">TPSA / ընդ.պրոստատ սպեցիֆիկ հակածին/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->TPSA ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">FPSA / ազատ պրոստատ սպեցիֆիկ հակածին/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->FPSA ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">CEA / կարցինոէմբրիոնալ հակածին/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->CEA ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">CA19-9 / կարբոհիդրատ հակածին 19-9/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->CA19 ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">CA15-3 / ուռուցքային հակածին 15-3/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->CA15 ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">CA125 / ուռուցքային հակածին 125/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->CA15 ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">CA 72-4 / կարբոհիդրատ հակածին 72-4/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->CA72 ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">NSE / նեյրոսպեցիֆիկ էնոլազա/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->NSE ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">Cyfra 21-2 / ցիտոկերատինի մասնիկ 21-2/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->Cyfra ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">b-hCG / բետա խորիոնիկ հոնադոթրոպին/</div>
                        <div class="col-4">{{optional($immunologicalPattern)['b-hCG'] ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">SCC / տափակբջջային ուռուցքային հակածին/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->SCC ?? '--'}}</div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-7">b-2MG / բետա -2միկրոգլոբուլին/</div>
                        <div class="col-4">{{optional($immunologicalPattern)->SCC ?? '--'}}</div>
                    </div>
                    <hr class="hr-dashed">

                @empty
                @endforelse
        </li>


        <li class="list-group-item text-primary">
            <div class="row">
                <div class="col-md-7">
                    <strong>
                        Ուռուցքի տեսանելի տեղակայում՝
                    </strong>

                </div>
                <div class="col-md-5">
                    <x-forms.checkbox-radio pos="align" id="pmmnd-radio1" value="1" {{--old-default="{{$stationary->is_urgent}}"--}}
                    class="tvl-radio" name="tumor_visible_location" label="Այո" />
                    <x-forms.checkbox-radio pos="align" id="pmmnd-radio2" value="0" {{--old-default="{{$stationary->is_urgent}}"--}}
                    class="tvl-radio" name="tumor_visible_location" label="Ոչ" />
                </div>
                <div class="col-md-12">
                    <em class="error text-danger" data-input="tumor_visible_location"></em>
                </div>
            </div>
        </li>

        <li class="list-group-item">
            <h5>Ուղեկցող հիվանդություններ</h5>
            @forelse ($stationaries as $stationary)
                {{-- @dump($stationary->number .' -- '. $stationary->admission_date) --}}
                <strong>Ստացիոնար {{__('patient.patient')}} բժշկական քարտ №{{$stationary->number}}</strong>

                @forelse ($stationary->stationary_diagnoses->where('diagnosis_type',"concomitant_disease") as $concomitant_diagnosis)
                @php
                    $concomitant_disease_code = optional($concomitant_diagnosis)->disease_item->code ?? '--';
                    $concomitant_disease_name = optional($concomitant_diagnosis)->disease_item->name ?? '--';
                    $concomitant_diagnosis_created_at = optional($concomitant_diagnosis)->created_at ?? '--';
                    $concomitant_diagnosis_comment = optional($concomitant_diagnosis)->diagnosis_comment ?? '--';
                @endphp
                <div class="container">
                    <div class="row justify-content-end">
                        {{-- @dump( $stationary_concomitant_disease) --}}
                        <div class="col-3">
                            <div class="rounded-sm  border-bottom p-2">{{$concomitant_diagnosis_created_at}}</div>
                        </div>
                        <div class="col-8">
                            <div class="rounded-sm  border-bottom p-2">{{$concomitant_disease_code .' - '. $concomitant_disease_name}}</div>
                        </div>
                        <div class="col-8">
                            <div class="rounded-sm  border-bottom p-2">{{$concomitant_diagnosis_comment}}</div>
                        </div>
                    </div>
                </div>
                @empty
                <span class="mx-2"> -- </span><br>
                @endforelse
            @empty
            @endforelse
        </li>

        <li class="list-group-item">
            <h5>Ռիսկի գործոններ</h5>
            @forelse ($stationaries as $stationary)
                {{-- @dump($stationary->number .' -- '. $stationary->admission_date) --}}
                <strong>Ստացիոնար {{__('patient.patient')}} բժշկական քարտ №{{$stationary->number}}</strong>
                @forelse ($stationary->stationary_harmfuls as $item)
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-3">
                            <div class="rounded-sm  border-bottom p-2">{{$item->created_at ?? "--"}}</div>
                        </div>
                        <div class="col-8">
                            <div class="rounded-sm  border-bottom p-2">{{$item->harmful->name ?? "--"}}</div>
                        </div>
                        <div class="col-8">
                            <div class="rounded-sm  border-bottom p-2">{{$item->harmful_comment ?? "--"}}</div>
                        </div>
                    </div>
                </div>
                @empty
                <span class="mx-2"> -- </span><br>
                @endforelse
            @empty
            @endforelse
        </li>

        <!-- վեջացնել, դառնալ սրան -->
        <h4 class="text-center my-4">Հիվանդության դասակարգում</h4>
        <li class="list-group-item">
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար հիվանդի բժշկական քարտ №{{$stationary->number}}</strong>
                <div class="row justify-content-end">
                    <div class="col-7">Ուռուցքի դասակարգման/փուլի ամսաթիվը</div>
                    <div class="col-4">{{Carbon\Carbon::create($stationary->admission_date)->format('d.m.Y') ?? '--'}}</div>
                </div>
            @empty
            @endforelse
        </li>
        <li class="list-group-item">
            <strong>Կլինիկական խումբ</strong>
            @forelse ($cancer_groups as $cancer_group)
                <div class="row justify-content-end">
                    <div class="col-5">{{$cancer_group->name ?? '--'}}</div>
                    <div class="col-4">{{Carbon\Carbon::create($cancer_group->pivot_created_at)->format('d.m.Y') ?? '--'}}</div>
                </div>
            @empty
            @endforelse
        </li>
        <li class="list-group-item">
            <h5 class="text-warning">TNM դասակարգում</h5>
        </li>

        <h4  class="text-center my-4">Բուժում</h4>
        <li class="list-group-item">
            <h5 class="text-primary text-bold">Բուժումը չիրականացնելու պատճառը</h5>
            <div class="form-row">
                <div class="col-md-4">
                    {{-- <strong>Ամսաթիվ՝</strong> --}}
                    <x-forms.text-field type="date" name="not_carrying_out_treatment_reason_date" validationType="ajax"
                    value='{{old("not_carrying_out_treatment_reason_date")}}'/>
                </div>
                <div class="col-md-8">
                    {{-- <strong>Հնարավոր տարբերակներ՝</strong> --}}
                    <x-forms.magic-search class="magic-search ajax"
                        data-catalog-name="not_carrying_out_treatment_reason_lists"
                        hidden-id="not_carrying_out_treatment_reason_id" hidden-name="not_carrying_out_treatment_reason_id"
                        placeholder="Ընտրել բուժումը չիրականացնելու պատճառը․․․"
                        {{-- value='{{old("application_purpose_id", $patient->working_feature_id)}}' --}}
                        />
                        <em class="error text-danger" data-input="not_carrying_out_treatment_reason_id"></em>
                </div>
            </div>
        </li>

        <li class="list-group-item">
            <h5 class="text-primary">Բուժումը իրականացվել է</h5>
            <div class="form-row">
                <div class="col-md-4">
                    {{-- <strong>Ամսաթիվ՝</strong> --}}
                    <x-forms.text-field type="date" name="treatment_was_carried_out_date" validationType="ajax"
                    value='{{old("treatment_was_carried_out_date")}}'/>
                </div>
                <div class="col-md-8">
                    {{-- <strong>Հնարավոր տարբերակներ՝</strong> --}}
                    <x-forms.magic-search class="magic-search ajax"
                        data-catalog-name="treatment_was_carried_out_lists"
                        hidden-id="treatment_was_carried_out_id" hidden-name="treatment_was_carried_out_id"
                        placeholder="Ընտրել բուժման իրականացման ռեժիմը․․․"
                        {{-- value='{{old("application_purpose_id", $patient->working_feature_id)}}' --}}
                        />
                        <em class="error text-danger" data-input="treatment_was_carried_out_id"></em>
                </div>
            </div>
        </li>

        <li class="list-group-item">
            <h5>Բուժման տեսակ <small>(14.1 Բուժման այլ տեսակներ)</small></h5>
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար հիվանդի բժշկական քարտ №{{$stationary->number}}</strong>
                @forelse ($stationary->stationary_treatments as $item)
                    <div class="container">
                        <div class="row justify-content-end">
                            <div class="col-5">
                                <div class="rounded-sm  border-bottom p-2">{{$item->treatment_item->name ?? ""}}</div>
                            </div>
                            <div class="col-6">
                                <div class="rounded-sm  border-bottom p-2">{{$item->treatment_comment ?? ""}}</div>
                            </div>
                        </div>
                    </div>
                @empty
                <span class="mx-2"> -- </span><br>
                @endforelse
            @empty
            @endforelse
        </li>

        <li class="list-group-item">
            <h5>Բուժական միջամտություն <small>(14.2 Չարորակ նորագոյություններով հիվանդների համար)</small></h5>
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար հիվանդի բժշկական քարտ №{{$stationary->number}}</strong>
                @forelse ($stationary->tumor_treatments as $item)
                    <div class="container">
                        <div class="row justify-content-end">
                            <div class="col-3">
                                <div class="rounded-sm  border-bottom p-2">
                                    {{ __("enums.tumor_treatment_enum.$item->type") }}
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="rounded-sm  border-bottom p-2">
                                    {{$item->name ?? ""}}
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                <span class="mx-2"> -- </span><br>
                @endforelse
            @empty
            @endforelse
        </li>

        <li class="list-group-item">
            <h5>Վիրահատության տեսակ</h5>
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար հիվանդի բժշկական քարտ №{{$stationary->number}}</strong>
                @forelse ($stationary->stationary_surgeries->where('type', StationarySurgeryEnum::stationary()) as $item)
                    <div class="container">
                        <div class="row justify-content-end">
                            <div class="col-3">
                                <div class="rounded-sm  border-bottom p-2">{{$item->surgery_date ?? ""}}</div>
                            </div>
                            <div class="col-8">
                                <div class="rounded-sm  border-bottom p-2">{{$item->surgery->name ?? ""}}</div>
                            </div>
                        </div>
                    </div>
                @empty
                <span class="mx-2"> -- </span><br>
                @endforelse
            @empty
            @endforelse
        </li>

        <li class="list-group-item">
            <h5>Բարդություն <small>(11.բ) հիմնական հիվանդության բարդություն)</small></h5>
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար հիվանդի բժշկական քարտ №{{$stationary->number}}</strong>
                @forelse ($stationary->stationary_diagnoses->where("diagnosis_type",
                            StationaryDiagnosisEnum::disease_complication()) as $item)
                    <div class="container">
                        <div class="row justify-content-end">
                            <div class="col-3">
                                <div class="rounded-sm  border-bottom p-2">Բարդության անվանում՝</div>
                            </div>
                            <div class="col-8">
                                <div class="rounded-sm  border-bottom p-2">{{$item->disease_item->code_name ?? ""}}</div>
                            </div>
                            <div class="col-3">
                                <div class="rounded-sm  border-bottom p-2">Ազատ լրացում՝</div>
                            </div>
                            <div class="col-8">
                                <div class="rounded-sm  border-bottom p-2">{{$item->diagnosis_comment ?? ""}}</div>
                            </div>
                        </div>
                    </div>
                @empty
                <span class="mx-2"> -- </span><br>
                @endforelse
            @empty
            @endforelse
        </li>


        <li class="list-group-item">
            <h5>Բուժման արդյունավետություն</h5>
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար հիվանդի բժշկական քարտ №{{$stationary->number}}</strong>
                @php
                    $ste = $stationary->stationary_treatment_evaluation;
                    $treatment_effectiveness = $ste->treatment_effectiveness ?? null;
                @endphp
                <div class="container">
                    <div class="row justify-content-end">
                        @if ( $treatment_effectiveness)
                        <div class="col-12">
                            <div class="rounded-sm  border-bottom p-2">
                                {{__("enums.treatment_effectiveness_enum.$treatment_effectiveness")}}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            @empty
            @endforelse
        </li>

        <li class="list-group-item">
            <h5>Մահվան պատճառ</h5>
            @forelse ($stationaries as $stationary)
                <strong>Ստացիոնար հիվանդի բժշկական քարտ №{{$stationary->number}}</strong>
                @php
                    $spa = $stationary->stationary_pathological_anatomical;
                    // $cause_of_death = $spa->cause_of_death;
                @endphp
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-3">
                            <div class="rounded-sm  border-bottom p-2">{{$stationary->discharge_date ?? "--"}}</div>
                        </div>
                        <div class="col-8">
                            <div class="rounded-sm  border-bottom p-2">{{optional(optional($spa)->cause_of_death)->name}}</div>
                        </div>
                    </div>
                </div>
            @empty
            @endforelse
        </li>


        <li class="list-group-item">
            <h5 class="text-warning">Բուժան հետ կապված ստաց․, քիմ, ճառ, օփիոիդային ձևերց բերվող ինֆորմացիա․․․</h5>
        </li>

        <h4 class="text-center my-4">Այլ տվյալներ</h4>
        <li class="list-group-item">
            <div class="col-md-4">
                <strong class="text-primary">Ախտորոշումը չի հաստատվել</strong>
                <x-forms.text-field type="date" name="diagnosis_hasnot_confirmed_date" validationType="ajax"
                value='{{old("diagnosis_hasnot_confirmed_date")}}'/>
            </div>
        </li>

        @include('shared.forms.list_group_item_submit', ['btn_text'=>'Ավելացնել'])
    </ul>

    <div class="my-5"></div>
</form>
</div>
@endsection


@section('javascript')
    <script src="{{ mix('js/jquery.js') }}"></script>
    <script src="{{ mix('js/all.magicsearch.js') }}"></script>

    <script>
        // ---------------Պացիենտի կցագրում-------------------//
        const clinicsUrl = @json(route('catalogs.clinics_json'));
        const regions = @json($regions);
        const regionNameDisabledInput = $('#region_name');
        $('[id^="attached_clinic_id_search"]').magicsearch(
            window.medexMagicSearch.assignConfigs({
                type: "ajax",
                dataSource: clinicsUrl,
                fields: ["value","label","regional_code"],
                id: "value",
                format: "%label%", // %value% -
                success: function($input, data) {
                    // console.log(data)
                    // console.log($input)
                    const [{name=''}] = regions.filter(function(reg) {
                        return reg.code == data.regional_code
                    });
                    regionNameDisabledInput.val(name);

                    const hidden_input_id = $input.data("hidden");
                    $(hidden_input_id).val($input.attr("data-id"));
                },
                afterDelete: function($input, data) {
                    regionNameDisabledInput.val('');

                    const multiple = $input.data("multiple");
                    const hidden_input_id = $input.data("hidden");
                    multiple ?  $(hidden_input_id).val($input.attr("data-id")) : $(hidden_input_id).val("");
                }
            })
        );

        // -------------------Առաջնակի բազմակի--------------------//
        const $synchronous_radio_group = $('.synchronous-radio-group');
        const $synchronous_radios = $('[name="synchronous"]');
        $('#pmmnd-radio1').change(function() {
            $synchronous_radio_group.show();
            $synchronous_radio_group.parent( ".list-group-item" ).css( "height", "auto" );
        });
        $('#pmmnd-radio2').change(function() {
            $synchronous_radio_group.hide();
            $synchronous_radio_group.parent( ".list-group-item" ).css( "height", "47px" );
            $synchronous_radios.prop('checked', false);
        });

        // <button class="click-me btn btn-primary">click</button>
        // $('.click-me').click(  function() {
        //     $synchronous_radios.prop('checked', false);
        // })
    </script>
@endsection
