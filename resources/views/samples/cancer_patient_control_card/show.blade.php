
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{mix('css/print/cancer_patient_control_card.css')}}">
    <title>Քաղցկեղով {{__('patient.patient')}} հսկողության քարտ</title>
</head>
<body>
<div class="page-wrap">
    <div class="main-container">
        <div class="text-center"><strong>Քաղցկեղով հիվանդի հսկողության քարտ</strong></div>
        <br><br><br>
            <div>Պացիենտի կցագրում</div>
        <div class="display-flex">
               <p>{{ $cpcc->attached_clinic->name ?? null}}</p>
            <p class="margin-left">{{$cpcc->attached_clinic->region->name ?? null}}</p>
        </div>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div>Ազգանուն, անուն, հայրանուն</div>
                <span class="bottom-line">{{$cpcc->patient->f_name ?? null}} {{$cpcc->patient->l_name ?? null}} {{$cpcc->patient->p_name ?? null}}</span>
            </div>
            <div class="display-flex margin-left">
                <div>Սեռը</div>
                <span class="bottom-line">
                    {{$cpcc->patient->Sex ?? null}}
                </span>
            </div>
        </div>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div> Ծննդյան ամսաթիվ</div>
                <span class="bottom-line">{{$cpcc->user->birth_date ?? null}}</span>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="display-flex">
                <div>ՀԾՀ</div>
                <span class="bottom-line">{{$cpcc->user->soc_card ?? null}}</span>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="display-flex">
                <div>Անձնագիր</div>
                <span class="bottom-line">{{$cpcc->user->passport ?? null}}</span>
            </div>
        </div>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div>Քաղաքացիություն</div>
                <span class="bottom-line">{{$cpcc->patient->citizenship ?? null}}</span>
            </div>
            <div class="display-flex margin-left">
                <div> Ազգություն</div>
                <span class="bottom-line">{{$cpcc->user->nationality ?? null}}</span>
            </div>
        </div>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div> Սոցիալ կենցաղային պայմաններ</div>
                <span class="bottom-line">{{$cpcc->patient->social_living_condition->name ?? null}}</span>
            </div>
            <div class="display-flex margin-left">
                <div>Ամուսնական կարգավիճակ</div>
                <span class="bottom-line">{{$cpcc->patient->marital_status->name ?? null}}</span>
            </div>
        </div>
        <br>
        <div class="display-flex">
            <div>Աշխատանքային առանձնահատկություններ</div>
            <span class="bottom-line">{{$cpcc->patient->working_feature->name ?? null}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div>Կրթություն</div>
            <span class="bottom-line">{{$cpcc->patient->education->name ?? null}}</span>
        </div>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div>էլ․ փոստ</div>
                <span class="bottom-line">{{$cpcc->user->email ?? null}}</span>
            </div>
            <div class="display-flex margin-left">
                <div>Տարիք</div>
                <span class="bottom-line">{{$cpcc->patient->hasAge() ? $cpcc->patient->age :''}}</span>
            </div>
        </div>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div>Տարիքը հայտնաբերման տարում</div>
                <span class="bottom-line">{{$cpcc->age_at_discovery_year ?? null}} {{$cpcc->age_type_at_discovery_year ?? null}}</span>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="display-flex">
                <div>Տարիքային կոդ հայտնաբերման տարում</div>
                <span class="bottom-line">{{$cpcc->age_code_at_discovery_year ?? null}}</span>
            </div>
        </div>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div>Քաղաքային հեռ․</div>
                <span class="bottom-line">{{$cpcc->patient->c_phone ?? ''}}</span>
            </div>
            <div class="display-flex margin-left">
                <div>Բջջային հեռ․</div>
                <span class="bottom-line">{{$cpcc->patient->m_phone ?? ''}}</span>
            </div>
        </div>
        <br>
        <div class="display-flex">
            <div>Բնակության վայր</div>
            <span class="bottom-line">{{$cpcc->patient->living_place->name ?? ''}}</span>
        </div>
        <br>
        <h4>Գրանցման Հասցե</h4>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div>Մարզ</div>
                <span class="bottom-line">{{$cpcc->patient->residence_region ?? ''}}</span>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="display-flex">
                <div>Քաղաք/Գյուղ</div>
                <span class="bottom-line">{{$cpcc->patient->town_village ?? ''}}</span>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="display-flex">
                <div>Փողոց/Շենք</div>
                <span class="bottom-line">{{$cpcc->patient->street_house ?? ''}}</span>
            </div>
        </div>
        <br>
        <h4>Բնակության Հասցե</h4>
        <br>
        <div class="display-flex">
            <div class="display-flex">
                <div>Մարզ</div>
                <span class="bottom-line">{{$cpcc->patient->residence_region_residence ?? ''}}</span>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="display-flex">
                <div>Քաղաք/Գյուղ</div>
                <span class="bottom-line">{{$cpcc->patient->town_village_residence ?? ''}}</span>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="display-flex">
                <div>Փողոց/Շենք</div>
                <span class="bottom-line">{{$cpcc->patient->street_house_residence ?? ''}}</span>
            </div>
        </div>
        <br>
        <h4>Սոց․ խումբ</h4>
        <br>
                @forelse ($stationaries as $stationary)
                    <div>Ստացիոնար պացիենտի բժշկական քարտ №<span class="bottom-line">{{$stationary->number}}</span></div>
                &nbsp
            <br>
            @forelse ($stationary->stationary_social_packages as $stationary_social_package)

                <span class="bottom-line">{{optional($stationary_social_package->package_item)->created_at ?? '--'}}</span>
                <span class="bottom-line">{{optional($stationary_social_package->package_item)->name ?? '--'}}</span>
                <br><br>
                @empty
                @endforelse
        @empty
        @endforelse
        <br><br>
        <div class="display-flex">
            <div>Առաջնակի բազմակի տեղակայման չարորակ նորագոյացություն՝</div>
            <span class="bottom-line">
                @if($cpcc->primary_multiple_malignant_neoplasm_deployment == 1)
                    Այո
                @elseif($cpcc->primary_multiple_malignant_neoplasm_deployment == 0)
                    Ոչ
                @else

                @endif
            </span>
        </div>
        <br>
        @if($cpcc->primary_multiple_malignant_neoplasm_deployment == 1)
            @if($cpcc->synchronous == 1)
                <span>Միաժամանակյա (Սինքրոն)՝</span>
            @elseif($cpcc->synchronous == 0)
                <span class="bottom-line">Ոչ միաժամանակյա (Մետաքրոն)՝ </span>
            @else

            @endif

        @endif
        <div class="display-flex">

        </div>
        <br><br>
        <div class="text-center">
            <strong>Հետազոտություններ, Հայտնաբերման եղանակներ</strong>
        </div>
        <br>
            <div class="display-flex">
                <div>Դիմելու նպատակը՝</div>
                <span class="bottom-line">{{ $cpcc->application_purpose->name ?? '' }}</span>
            </div>
        <br>
            <div class="display-flex margin-left1">
                <div>Հաշվառում ՈՒԱԿ-ում՝</div>
                <span class="bottom-line">{{ $cpcc->cpcc_registration_date ?? '' }}</span>
            </div>
        <br>
        <div class="display-flex">
            <div>Հաշվառման վերցնելու տարբերակները՝</div>
            <span class="bottom-line">{{ $cpcc->registration_option->name ?? '' }}</span>
        </div>
        <br>
        @forelse ($first_info as $info_item)
            <div class="display-flex">
                <div>Մինչ ախտորոշման հաստատվելը, որտեղ է հետազոտվել՝</div>
                <span class="bottom-line">{{$info_item->first_clinic_item->name  ?? ''}}</span>
            </div>
            <br>
            <div class="display-flex">
                <div>Մինչ ախտորոշման հաստատվելը, երբ է հետազոտվել՝</div>
                <span class="bottom-line">{{$info_item->first_clinic_date ?? ''}}</span>
            </div>
        @empty
        @endforelse
        <br>
        @forelse ($first_info as $info_item)
            <div class="display-flex">
                <div>Որտեղ է առաջին անգամ ախտորոշվել՝</div>
                <span class="bottom-line">{{$info_item->first_discovered_item->name  ?? ''}}</span>
            </div>
            <br>
            <div class="display-flex">
                <div>Երբ է առաջին անգամ ախտորոշվել ՝</div>
                <span class="bottom-line">{{$info_item->first_discovered_date ?? ''}}</span>
            </div>
        @empty
        @endforelse
        <br>
        <div class="text-center">
            <strong>Ախտորոշում</strong>
        </div>
        <br><br>
        <div>Լոկալիզացիա (Ստացիոնար՝ Հիմնական հիվանդության ախտորոշումը՝ գրվում է դուրս գրումից հետո)</div>
        <br>
        <div>Ստացիոնար պացիենտի  բժշկական քարտ № <span class="bottom-line">{{$stationary->number}}</span></div>
        <br>
        <p>fgf</p>
        <br>
        <strong>Հյուսվածքաբանական կառուցվածք</strong>
        <br>
        <br>
            <div>Ինչ հետազոտությամբ է հաստատվել ախտորոշումը</div>
        <br>
            <div class="bottom-line">Bjj</div>
        <br><br><br>
        <strong>Օնկոմարկեր</strong>
        <br><br>
        <div class="display-flex">
            <div>Ուռուցքի տեսանելի տեղակայում՝</div>
            <span class="bottom-line">ha</span>
        </div>
        <br>
        <strong>Ուղեկցող հիվանդություններ</strong>
        <br><br>
            <div>Ստացիոնար հիվանդի բժշկական քարտ ՝№ <span class="bottom-line">1</span></div>
        <br>
        <p>fgf</p><br>
        <strong>Ռիսկի գործոններ</strong>
        <br>
        <br>
            <div>Ստացիոնար հիվանդի բժշկական քարտ ՝№ <span class="bottom-line">1</span></div>
        <br>
        <p>fgf</p>
        <br><br>
        <div class="text-center">
            <strong>Բուժում</strong>
        </div>
        <br>
        <div class="display-flex">
            <div>Բուժումը չիրականացնելու պատճառը</div>
            <span class="bottom-line">chhamadzaynvav</span>
        </div>
        <br>
        <div class="display-flex">
            <div>Ամսաթիվ</div>
            <span class="bottom-line">10-10-2020</span>
        </div>
        <br>
        <div>Բուժումը իրականացվել է</div>
        <br>
        <div class="display-flex">
            <div>Ամսաթիվ</div>
            <span class="bottom-line">10-10-2020</span>
        </div>
        <br>
        <div class="display-flex">
            <div>Բուժման իրականացման ռեժիմը</div>
            <span class="bottom-line">սւսւսււսւս</span>
        </div>
        <br>
        <div class="text-center">
            <strong>Այլ տվյալներ</strong>
        </div>
        <br>
        <div class="display-flex">
            <div>Ախտորոշմը չի հաստատվել</div>
            <span class="bottom-line">10-10-2020</span>
        </div>
    </div>
</div>
</body>
</html>
