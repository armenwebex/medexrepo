@extends('layouts.cardBase')

@section('css')
    <link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet"/>
@endsection

@section('card-header')
@section('card-header-classes', '')
    <div class="text-center">
        <h3>Քաղցկեղով {{__('patient.patient')}} հսկիչ քարտ |3|</h3>
    </div>
@endsection

@section('card-content')
<div class="container">
    <ul class="list-group">
        <li class="list-group-item">
            <h5 class="text-primary">Պացիենտի կցագրում</h5>
            <div class="form-row">
                <div class="col-md-6">
                    <x-forms.magic-search
                    hidden-name="attached_clinic_id" hidden-id="attached_clinic_id"
                    id="attached_clinic_id_search" class="my-2"
                    placeholder="Ընտրել բուժ․ հաստատության անվանումը․․․"
                    value='{{old("attached_clinic_id")}}'/>
                </div>
                <div class="col-md-6">
                        <x-forms.text-field type="text" class="my-2 font-weight-bold" id="region_name" name="region_name"
                        value='{{old("region_name")}}' disabled/>
                        {{-- <x-forms.text-field type="hidden" id="region_id" name="region_id"
                        value='{{old("region_id")}}' disabled/> --}}
                </div>
            </div>
        </li>
    </ul>
</div>
@endsection

@section('javascript')
    <script src="{{ mix('js/jquery.js') }}"></script>
    <script src="{{ mix('js/all.magicsearch.js') }}"></script>

    <script>
        const clinicsUrl = @json(route('catalogs.clinics_json'));
        const regions = @json($regions);
        const regionNameDisabledInput = $('#region_name');


        // https://www.javatpoint.com/es6-array-destructuring
        $('[id^="attached_clinic_id_search"]').magicsearch(
            window.medexMagicSearch.assignConfigs({
                type: "ajax",
                dataSource: clinicsUrl,
                fields: ["value","label","regional_code"],
                id: "value",
                format: "%value% - %label%",
                success: function($input, data) {
                    console.log(data)
                    console.log($input)
                    const [{name=''}] = regions.filter(function(reg) {
                        return reg.code == data.regional_code
                    });
                    console.log(name)
                    regionNameDisabledInput.val(name);
                },
                afterDelete: function($input, data) {
                    regionNameDisabledInput.val('');
                }
            })
        );
    </script>
@endsection
