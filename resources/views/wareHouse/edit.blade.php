@extends('layouts.cardBase')

@php

        @endphp

@section('css')
<link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet" />
@endsection

@section('card-header')
@section('card-header-classes', 'text-center')
<h2>Պահեստ</h2>
@endsection

@section('card-content')
    @if(session('updated'))
        <div class="alert alert-success">
            {{session('updated')}}
        </div>
    @endif

<div class="container">
    <form method="post" action="{{route('wareHouse.warehouses.update',$warehouses->id )}}">
        @method('put')
        @csrf()
        <div class="form-group">
            <div class="row">
                <div class="col-12">
                    <label for="name">Ելքի հաշիվ, ենթահաշիվ</label>
                    <input type="text" name="exit" id="exit" value="{{$warehouses->exit}}" class="form-control" placeholder="Տեսակը">
                    <br>
                </div>
                <div class="col-12">
                    <label for="name">Անվանումը, բնութագիրը</label>
                    <input type="text" name="name" id="name" value="{{$warehouses->NMV->name ?? ' '}}" class="form-control" placeholder="Տեսակը">
                    <br>
                </div>
                <div class="col-12">
                    <label for="name">Ծածկագիրը</label>
                    <input type="text" name="code" id="code" value="{{$warehouses->code}}" class="form-control" placeholder="Տեսակը">
                    <br>
                </div>
                <div class="col-12">
                    <label for="name">Չափման միավորը</label>
                    <input type="text" name="unit" id="unit" value="{{$warehouses->NMV->unit ?? ''}}" class="form-control" placeholder="Տեսակը">
                    <br>
                </div>
                <div class="col-12">
                    <label for="name">Բաց թողնված քանակը</label>
                    <input type="text" name="quantity" id="quantity" value="{{$warehouses->quantity}}" class="form-control" placeholder="Տեսակը">
                    <br>
                </div>
                <div class="col-12">
                    <label for="name">Միավորի արժեքը</label>
                    <input type="text" name="price" id="price" value="{{ $warehouses->price}}" class="form-control" placeholder="Տեսակը">
                    <br>
                </div>
            </div>
            </div>
        <div class="form-group mt-2">
            <button type="submit" class="btn btn-primary">Թարմացնել</button>
        </div>
    </form>
</div>
<hr class="my-4">


@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/all.magicsearch.js') }}"></script>
<script src="{{ mix('/js/components/Select.js')}}"></script>

@endsection
