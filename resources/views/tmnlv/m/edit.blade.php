@extends('layouts.cardBase')

@php

@endphp

@section('css')
<link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet" />
@endsection

@section('card-header')
@section('card-header-classes', 'text-center')
<h2>M</h2>
@endsection

@section('card-content')
    @if(session('updated'))
        <div class="alert alert-success">
            {{session('updated')}}
        </div>
    @endif
<div class="container">
    <form method="post" action="{{ route('m.update', [$m->id]) }}">
        @method('put')
        @csrf()
        <div class="form-group">
            <div class="row">
                <div class="col-12">
                    <label for="name">Փոխել N</label>
                    <input type="text" name="name" id="name" value="{{ $m->name }}" class="form-control" placeholder="Տեսակը">
                </div>
            </div>
            </div>
        <div class="form-group mt-2">
            <button type="submit" class="btn btn-primary">Թարմացնել</button>
        </div>
    </form>
<hr class="my-4">


@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/all.magicsearch.js') }}"></script>
<script src="{{ mix('/js/components/Select.js')}}"></script>


@endsection
