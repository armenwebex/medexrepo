@extends('layouts.AdminCardBase')

@section('css')
<link href="{{ mix('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('card-header')

    <div class="card-header-actions">
        <h4>Նոր Ուղեգիր</h4>

    </div>
    <div class="card-header-actions">
        <a href="{{ route("admin.directions.create") }}" class="btn btn-primary float-right mr-4" type="button" target="_blank">
            <x-svg icon="cui-plus" />
            Նոր Ուղեգիր
        </a>
    </div>

@endsection

@section('card-content')

    <table class="table table-striped table-bordered datatable-default">
        <thead>
        <tr>
            <th>ID</th>
            <th>Անվանում</th>
            <th>Բաշխումը</th>
            <th>Հաստատումը</th>
            <th>Գործողություններ</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Mary</td>
            <td>Moe</td>
            <td>mary@example.com</td>
            <td>sadcsam</td>
            <td>sdfsd</td>
        </tr>
        <tr>
            <td>Mary</td>
            <td>Moe</td>
            <td>mary@example.com</td>
            <td>sadcsam</td>
            <td>sdfsd</td>
        </tr>
        </tbody>
    </table>
@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/datatables.js') }}"></script>

@endsection
