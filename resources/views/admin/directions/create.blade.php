@extends('layouts.AdminCardBase')

@section('css')
<link href="{{mix("/css/jquery.magicsearch.min.css")}}" rel="stylesheet" />
@endsection


@section('card-header')
@section('card-header-classes', '')
<div class="text-center">
    <h3>Ավելացնել բաժին</h3>
</div>
@endsection


@section('card-content')

<div class="container">
    <form action="{{route('admin.departments.store')}}" method="POST">
        @csrf

        <div class="form-row align-items-center my-2 mx-2">
            <div class="col-md-6">

                <div class="form-group">


                    <label for="title">Աշխատակից:</label>
                    <select name="doctor_id" id="doctors" class="form-control" style="width:350px">
                        <option value="">Ընտրել բուժող բժշկին</option>
                        @foreach($doctors as  $value)
                            <option value="{{$value->id}}">{{$value->f_name}}&nbsp;{{$value->l_name}} </option>
                        @endforeach
                    </select>
                </div>
              {{--  <strong>Աշխատակից</strong>

                <x-forms.magic-search class="doctors-search mt-1" placeholder="ընտրել բուժող բժշկին․․․"
                                      value=''
                                      hidden-id="attending_doctor_id" hidden-name="attending_doctor_id" />--}}
            </div>

            <div class="col-md-6">

                <div class="form-group">

                   <label for="title">Բաժանմունք:</label>
                  <select name="department" id="department" class="form-control" style="width:350px">
                    </select>
                </div>
            </div>

        </div>



        <div class="form-row align-items-center my-2 mx-2">
            <div class="col-md-6">
                <label for="title">Լաբորատոր ծառայություններ</label>
                <select name="" id="" class="form-control" style="width:350px">
                </select>
            </div>

         {{--   <div class="form-group">
                <label for="title">Select department:</label>
                <select name="department" id="department" class="form-control" style="width:350px">
                </select>
            </div>--}}

            @include('shared.forms.list_group_item_submit')
        </div>
    </form>
</div>

@endsection

@section('javascript')
<script src="{{ mix('js/jquery.js') }}"></script>
<script src="{{ mix('js/all.magicsearch.js') }}"></script>

<script>
$(document).ready(function () {
    $('#doctors').change(function(){

    var doctorID = $(this).val();
    if(doctorID){
        $.ajax({
            type:"GET",
            url:"{{url('admin/directions/getDepartmentByUserId')}}?doctor_id="+doctorID,
            success:function(res){
                if(res){
                    console.log(res);
                    $("#department").empty();
                    $("#department").append('<option>Select</option>');
                    $.each(res,function(key,value){
                        $("#department").append('<option value="'+key+'">'+value+'</option>');
                    });

                }else{
                    $("#department").empty();
                }
            }
        });
    }else{
        $("#department").empty();
        //   $("#city").empty();
    }
    });

 /*   $('.doctors-search').magicsearch(
        window.medexMagicSearch.assignConfigs({
            dataSource: "/lists/users.json",
            type: "ajax",
            success: function($input, data) {
                var doctorID = data.value;
                const hidden_input_id = $input.data('hidden');
                $(hidden_input_id).val($input.attr("data-id"));


                $.ajax({
                    type:"GET",
                    url:"{{url('admin/directions/getDepartmentByUserId')}}?doctor_id="+doctorID,
                    success:function(res){
                        if(res){
                            $("#department").empty();
                            $("#department").append('<option>Select</option>');
                            $.each(res,function(key,value){
                                $("#department").append('<option value="'+key+'">'+value+'</option>');
                            });

                        }else{
                            $("#department").empty();
                        }
                    }
                });
            }
        })
    );*/

   /* $('#department').on('change',function(){
        var departmentID = $(this).val();
        if(departmentID){
            $.ajax({
                type:"GET",
                url:"{{url('api/get-city-list')}}?department_id="+departmentID,
                success:function(res){
                    if(res){
                        $("#city").empty();
                        $.each(res,function(key,value){
                            $("#city").append('<option value="'+key+'">'+value+'</option>');
                        });

                    }else{
                        $("#city").empty();
                    }
                }
            });
        }else{
            $("#city").empty();
        }

    });*/

 /*   $('.clinics-search').magicsearch(
        window.medexMagicSearch.assignConfigs({
            dataSource: '/catalogs/clinics.json',
            type:'ajax',
            success: function($input, data) {
                const hidden_input_id = $input.data('hidden');
                $(hidden_input_id).val($input.attr('data-id'));

                var departmentID = data.value;
                if(departmentID){
                    $.ajax({
                        type:"GET",
                        url:"{{url('api/get-city-list')}}?department_id="+departmentID,
                        success:function(res){
                            if(res){
                                $("#city").empty();
                                $.each(res,function(key,value){
                                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                                });

                            }else{
                                $("#city").empty();
                            }
                        }
                    });
                }else{
                    $("#city").empty();
                }
            }
        })
    );*/
});
</script>
@endsection
