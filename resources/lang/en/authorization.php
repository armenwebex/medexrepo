<?php

return [

    "user" => [
        "not-belongs-to-user" => "This component is not linked to Your account",
        "can-not-view-patients" => "You can not view the list of patients",
    ]

];
