<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationaryPathologicalAnatomicalDiagnosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stationary_pathological_anatomicals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('stationary_id');

            $table->date('autopsy_date')->nullable()->comment('դիահերձման ամսաթիվ');
            $table->string('autopsy_protocol')->nullable()->comment('դիահերձման արձանագրություն - համար');
            $table->unsignedTinyInteger('cause_of_death_list_id')->nullable()->comment('մահվան պատճառը ցուցակից');
            $table->text('pathological_anatomical_epicrisis')->nullable()->comment('ախտաբանա-անատոմիական էպիկրիզ - ազատ դաշտ');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('stationary_id')->references('id')->on('stationaries')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('cause_of_death_list_id', 'spa_cause_of_death_list_id_foreign')->references('id')->on('cause_of_death_lists');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stationary_pathological_anatomical_diagnoses');
    }
}
