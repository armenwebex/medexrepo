<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdviceSheetInsuranceResearchesListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advice_sheet_insurance_researches_lists', function (Blueprint $table) {
            $table->id();
            //$table->unsignedBigInteger('r_l_id')->unsigned();
            $table->unsignedBigInteger('a_s_i_id')->unsigned();
            $table->unsignedTinyInteger('researches_id')->unsigned();

            $table->foreign('a_s_i_id')->references('id')->on('advice_sheet_insurances');
            $table->foreign('researches_id')->references('id')->on('researches_lists');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
