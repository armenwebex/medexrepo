<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistologicalExaminationHistologicalStructureListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histological_examination_histological_structure_list', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('histological_structure_list_id');
            $table->unsignedBigInteger('histological_examination_id');
            $table->timestamps();

            $table->foreign('histological_structure_list_id', 'histological_structure_list_id_foreign')->references('id')->on('histological_structure_lists');
            $table->foreign('histological_examination_id', 'histological_examination_id_foreign')->references('id')->on('histological_examinations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histological_examination_histological_structure_list');
    }
}
