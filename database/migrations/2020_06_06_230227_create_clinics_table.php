<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinics', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->smallInteger('regional_code');
            $table->string('code', 20)->nullable();
            $table->text('name', 400);
            $table->enum('status',['active','inactive'])->default('active');

            $table->foreign('regional_code')->references('code')->on('region_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinics');
    }
}
