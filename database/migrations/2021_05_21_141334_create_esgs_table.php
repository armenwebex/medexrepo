<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEsgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('esgs', function (Blueprint $table) {
            $table->id();
            $table->integer('patient_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('electric')->nullable();
            $table->integer('history')->nullable();
            $table->integer('ambulator')->nullable();
            $table->timestamp('date')->nullable();
            $table->text('pq')->nullable();
            $table->text('qrs')->nullable();
            $table->text('qrst')->nullable();
            $table->text('rr')->nullable();
            $table->text('rhythm')->nullable();
            $table->text('sistolik')->nullable();
            $table->text('sistolikritm')->nullable();
            $table->text('p')->nullable();
            $table->text('r')->nullable();
            $table->text('q')->nullable();
            $table->text('s')->nullable();
            $table->text('st')->nullable();
            $table->text('t')->nullable();
            $table->text('voltage')->nullable();
            $table->text('inspiration')->nullable();
            $table->text('note')->nullable();
            $table->text('attending_doctor_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esgs');
    }
}
