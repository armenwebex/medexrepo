<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCpccResearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpcc_researches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cpcc_id');
            $table->unsignedTinyInteger('research_list_id');
            $table->timestamps();

            $table->foreign('cpcc_id')->references('id')->on('cancer_patient_control_cards');
            $table->foreign('research_list_id')->references('id')->on('researches_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpcc_researches');
    }
}
