<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCancerPatientControlCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancer_patient_control_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_id');
            $table->unsignedBigInteger('user_id');

            $table->unsignedSmallInteger('attached_clinic_id')->nullable()->comment('Կցագրում - բուժ․ հաստատության id-ն');

            $table->unsignedTinyInteger('age_at_discovery_year')->nullable()->comment('Տարիքը հայտնաբերման տարում');
            $table->string('age_type_at_discovery_year')->nullable()->comment(' Տարիքի տիպը հայտնաբերման տարում /օրեկան/ամսեկան/տարեկան');
            $table->unsignedTinyInteger('age_code_at_discovery_year')->nullable()->comment('Տարիքային կոդ հայտնաբերման տարում');

            $table->boolean('primary_multiple_malignant_neoplasm_deployment')->nullable()->comment('Առաջնակի բազմակի տեղակայման չարորակ նորագոյացություն');
            $table->boolean('synchronous')->nullable()->comment('Միաժամանակյա (Սինքրոն) || Ոչ միաժամանակյա (Մետաքրոն) - կախված է նախորդից');


            $table->unsignedTinyInteger('application_purpose_id')->nullable()->comment('Դիմելու նպատակը');
            $table->timestamp('cpcc_registration_date')->nullable()->comment('Հաշվառում ՈՒԱԿ-ում - ամսաթիվ');
            $table->unsignedTinyInteger('registration_option_id')->nullable()->comment('Հաշվառման վերցնելու տարբերակները');


            $table->boolean('tumor_visible_location')->nullable()->comment('Ուռուցքի տեսանելի տեղակայում');
            $table->unsignedTinyInteger('not_carrying_out_treatment_reason_id')->nullable()->comment('Բուժումը չիրականացնելու պատճառը');
            $table->timestamp('not_carrying_out_treatment_reason_date')->nullable()->comment('Բուժումը չիրականացնելու պատճառը նշելու ամսաթիվ');

            $table->unsignedTinyInteger('treatment_was_carried_out_id')->nullable()->comment('Բուժումը իրականացվել է այս կերպ');
            $table->timestamp('treatment_was_carried_out_date')->nullable()->comment('Բուժումը իրականացվել է այս ամսաթվին');


            $table->timestamp('diagnosis_hasnot_confirmed_date')->nullable()->comment('Ախտորոշմը չի հաստատվել');

            $table->timestamps();

            $table->foreign('attached_clinic_id')->references('id')->on('clinics');
            $table->foreign('application_purpose_id')->references('id')->on('application_purpose_lists');
            $table->foreign('registration_option_id')->references('id')->on('registration_option_lists');

            $table->foreign('not_carrying_out_treatment_reason_id', 'cpcc_not_carrying_out_treatment_reason_id')->references('id')->on('not_carrying_out_treatment_reason_lists');
            $table->foreign('treatment_was_carried_out_id', 'cpcc_treatment_was_carried_out_id')->references('id')->on('treatment_was_carried_out_lists');

            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancer_patient_control_cards');
    }
}
