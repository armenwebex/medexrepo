<?php

use Illuminate\Database\Seeder;
use App\Models\Intoloreance;

class IntoloreanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
           [ 'name' => 'Մետամիզոլ Նատրի'],
           [ 'name' => 'Ացետամինոֆեն'],
           [ 'name' => 'Ացետիլսալիցիլաթթու'],
           [ 'name' => 'Կապտոպրիլ'],
           [ 'name' => 'Ցեֆտրիասոն'],
           [ 'name' => 'Մետոկլոպրամիդ Հիդրոքլորիդ'],
        ];
        foreach ($data as $value) {
            Intoloreance::create($value);
        }
    }
}
