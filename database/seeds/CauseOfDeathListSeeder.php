<?php

use Illuminate\Database\Seeder;
use App\Models\CauseOfDeathList;

class CauseOfDeathListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['code' => '', 'name' => 'Մահացել է հիմնական հիվանդությունից'],
            ['code' => '', 'name' => 'Մահացել է վիրահատական բարդությունից'],
            ['code' => '', 'name' => 'Մահացել է բուժման բարդությունից'],
            ['code' => '', 'name' => 'Մահացել է այլ հիվանդությունից'],
            ['code' => '', 'name' => 'Մահացել է այլ պատճառով'],
        ];

        foreach ($data as $value) {
            CauseOfDeathList::create($value);
        }
    }
}
