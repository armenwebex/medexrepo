<?php

use Illuminate\Database\Seeder;
use App\Models\RegionList;

class RegionListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions_json = file_get_contents('./database/seeds/dumps/region-list.json');
        $regions = json_decode($regions_json, true);

        foreach ($regions as $item) {
            RegionList::create($item);
        }
    }
}
