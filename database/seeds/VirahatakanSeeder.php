<?php

use Illuminate\Database\Seeder;
use App\Models\Surgical;

class VirahatakanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $virahatakan_json = file_get_contents('./database/seeds/dumps/virahatakan.json');
        $virahatakan = json_decode($virahatakan_json, true);

        foreach ($virahatakan as $item) {
            Surgical::create($item);
        }
    }
}
