<?php

use Illuminate\Database\Seeder;
use App\Models\HistologicalStructureList;
use Illuminate\Support\Facades\File;

class HistologicalStructureListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # երկու տարբերակներն էլ աշխատող են։
        $json_data = file_get_contents('./database/seeds/dumps/histological-structure-list.json');
        // $json_data = File::get('./database/seeds/dumps/histological-structure-list.json');
        $json_decoded = json_decode($json_data, true);
        foreach ($json_decoded as $key => $item) {
            HistologicalStructureList::create($item);
        }
    }
}
