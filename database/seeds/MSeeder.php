<?php

use Illuminate\Database\Seeder;
use App\Models\M;

class MSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $m_json = file_get_contents('./database/seeds/dumps/tnm/m.json');
        $m_array = json_decode($m_json, true);

        foreach ($m_array as $item) {
            M::create($item);
        }
    }
}
