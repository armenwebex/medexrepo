<?php

use Illuminate\Database\Seeder;
use App\Models\Chemotherapy;

class QimiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qimia_json = file_get_contents('./database/seeds/dumps/qimia.json');
        $qimia = json_decode($qimia_json, true);

        foreach ($qimia as $item) {
            Chemotherapy::create($item);
        }
    }
}
