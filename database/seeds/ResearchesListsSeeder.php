<?php

use Illuminate\Database\Seeder;
use \App\Models\ResearchesList;

class ResearchesListsSeeder extends Seeder
{
    public function run()
    {
        $samples = [
            ['code' => '', 'name' => 'Հյուսվածքաբանական'],
            ['code' => '', 'name' => 'Բջջաբանական'],
            ['code' => '', 'name' => 'Ցիտոքիմիական'],
            ['code' => '', 'name' => 'Իմունոֆենոտիպավորում'],
            ['code' => '', 'name' => 'Կորիզա-մագնիսական-կոնտրաստով'],
            ['code' => '', 'name' => 'Կորիզա-մագնիսական-առանց կոնտրաստ'],
            ['code' => '', 'name' => 'Համակարգչային շերտագրություն-կոնտրաստով'],
            ['code' => '', 'name' => 'Համակարգչային շերտագրություն -առանց կոնտրաստ'],
            ['code' => '', 'name' => 'Ռենտգեն-առանց կոնտրաստ'],
            ['code' => '', 'name' => 'Ռենտգեն-կոնտրաստով'],
            ['code' => '', 'name' => 'Ուլտրաձայնային'],
            ['code' => '', 'name' => 'Էնդոսկոպիկ'],
            ['code' => '', 'name' => 'Իզոտոպ մեթոդով'],
            ['code' => '', 'name' => 'Միայն կլինիկորեն'],
            ['code' => '', 'name' => 'Օնկոմարկեր'],
            ['code' => '', 'name' => 'ՊԵՏ ԿՏ'],
            ['code' => '', 'name' => 'Իմելոգրաֆիա'],
            ['code' => '', 'name' => 'Ցիտոլոգիական'],
            ['code' => '', 'name' => 'Իմունոգենետիկական'],
            ['code' => '', 'name' => 'Ցիտոգենետիկ'],
            ['code' => '', 'name' => 'Դիահերձում'],
            ['code' => '', 'name' => 'Ախտորոշիչ վիրահատություն'],
            ['code' => '', 'name' => 'Այլ'],
        ];

        foreach ($samples as $samplesOther) {
            ResearchesList::create([
                "name" => $samplesOther['name'],
            ]);
        }
    }
}
