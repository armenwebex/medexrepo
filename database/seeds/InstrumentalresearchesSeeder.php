<?php
use App\Models\Instrumentalresearches;
use Illuminate\Database\Seeder;

class InstrumentalresearchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $instrumentalresearches_json = file_get_contents('./database/seeds/dumps/instrumentalresearches.json');
        $instrumentalresearches = json_decode($instrumentalresearches_json, true);

        foreach ($instrumentalresearches as $item) {
            Instrumentalresearches::create($item);
        }
    }
}
