<?php

use Illuminate\Database\Seeder;
use App\Models\Radiation;

class VirahatakantrueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $virahatakantrue_json = file_get_contents('./database/seeds/dumps/virahatakantrue.json');
        $virahatakantrue = json_decode($virahatakantrue_json, true);

        foreach ($virahatakantrue as $item) {
            Radiation::create($item);
        }
    }
}
