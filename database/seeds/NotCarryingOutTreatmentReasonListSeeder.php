<?php

use Illuminate\Database\Seeder;
use App\Models\NotCarryingOutTreatmentReasonList;

class NotCarryingOutTreatmentReasonListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['code' => '', 'name' => 'Հրաժարվել է բուժումից'],
            ['code' => '', 'name' => 'Ենթակա չէ հատուկ բուժման'],
            ['code' => '', 'name' => 'Ունեցել է հակացուցում'],
        ];

        foreach ($data as $value) {
            NotCarryingOutTreatmentReasonList::create($value);
        }
    }
}
