<?php

use Illuminate\Database\Seeder;
use App\Models\N;

class NSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $n_json = file_get_contents('./database/seeds/dumps/tnm/n.json');
        $n_array = json_decode($n_json, true);

        foreach ($n_array as $item) {
            N::create($item);
        }
    }
}
