<?php
use App\Models\Radiationresearches;
use Illuminate\Database\Seeder;

class RadiationresearchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $radiationresearches_json = file_get_contents('./database/seeds/dumps/radiationresearches.json');
        $radiationresearches = json_decode($radiationresearches_json, true);

        foreach ($radiationresearches as $item) {
            Radiationresearches::create($item);
        }
    }
}
