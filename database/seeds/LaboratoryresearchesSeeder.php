<?php
use App\Models\Laboratoryresearch;
use Illuminate\Database\Seeder;

class LaboratoryresearchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $laboratoryresearches_json = file_get_contents('./database/seeds/dumps/laboratoryresearches.json');
        $laboratoryresearches = json_decode($laboratoryresearches_json, true);

        foreach ($laboratoryresearches as $item) {
            Laboratoryresearch::create($item);
        }
    }
}
