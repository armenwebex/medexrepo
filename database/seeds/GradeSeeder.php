<?php

use Illuminate\Database\Seeder;
use App\Models\Grade;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grade_json = file_get_contents('./database/seeds/dumps/tnm/grade.json');
        $grade_array = json_decode($grade_json, true);

        foreach ($grade_array as $item) {
            Grade::create($item);
        }
    }
}
