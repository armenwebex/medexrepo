<?php


use Illuminate\Database\Seeder;
use \App\Models\Metastasis_list;

class MetastasisListSeeder extends Seeder
{

    public function run()
    {
        // $samples = [
        //     ["name" => "Գլխուղեղ"],
        //     ["name" => "Լյարդ"],
        //     ["name" => "Ոսկրեր"],
        //     ["name" => "Թոքեր"],
        //     ["name" => "Որովայնամիզ"],
        //     ["name" => "Մակերիկամ"],
        //     ["name" => "Երիկամ"],
        //     ["name" => "Մաշկ"],
        //     ["name" => "Փափուկ հյուսվածքներ"],
        //     ["name" => "Այլ տեղակայում + A"],
        // ];

        $metastasis_list = [
            ['name' => 'Ոսկրային հյուսվածք', 'code' => 'OSS'],
            ['name' => 'Թոքեր', 'code' => 'PUL'],
            ['name' => 'Լյարդ', 'code' => 'HEP'],
            ['name' => 'Գլխուղեղ', 'code' => 'BRA'],
            ['name' => 'Ավշային հանգույցներ', 'code' => 'LYM'],
            ['name' => 'Ոսկրածուծ', 'code' => 'MAR'],

            ['name' => 'Թոքամիզ', 'code' => 'PLE'],
            ['name' => 'Որովայնամիզ', 'code' => 'PER'],
            ['name' => 'Մակերիկամ', 'code' => 'ADR'],
            ['name' => 'Մաշկ', 'code' => 'SKI'],
            ['name' => 'Այլ տեղակայում', 'code' => 'OTH'],
        ];

        foreach ($metastasis_list as $metastasis) {
            Metastasis_list::create($metastasis);
        }
    }
}
