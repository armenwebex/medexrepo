<?php

use Illuminate\Database\Seeder;
use App\Models\Clinic;

class ClinicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clinics_json = file_get_contents('./database/seeds/dumps/clinics-with-region-code.json');
        $clinics = json_decode($clinics_json, true);
        foreach ($clinics as $item) {
            Clinic::create($item);
        }
    }
}
