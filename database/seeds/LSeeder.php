<?php

use Illuminate\Database\Seeder;
use App\Models\L;

class LSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $l_json = file_get_contents('./database/seeds/dumps/tnm/l.json');
        $l_array = json_decode($l_json, true);

        foreach ($l_array as $item) {
            L::create($item);
        }
    }
}
