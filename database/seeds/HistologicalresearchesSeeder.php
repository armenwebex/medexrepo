<?php
use App\Models\Histologicalresearches;
use Illuminate\Database\Seeder;

class HistologicalresearchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $histologicalresearches_json = file_get_contents('./database/seeds/dumps/histologicalresearches.json');
        $histologicalresearches = json_decode($histologicalresearches_json, true);

        foreach ($histologicalresearches as $item) {
            Histologicalresearches::create($item);
        }
    }
}
