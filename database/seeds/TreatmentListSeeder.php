<?php

use Illuminate\Database\Seeder;
use App\Models\TreatmentList;

class TreatmentListSeeder extends Seeder
{
    private $treatments = [
        "Կուրատիվ (բուժիչ) Վիրաբուժական արմատական",
        "Կուրատիվ (բուժիչ) Վիրաբուժական ոչ արմատական",
        "Կուրատիվ (բուժիչ) Թիրախային թերապիա",
        "Կուրատիվ (բուժիչ) Էնդոկրին (հորմոնալ)",
        "Կուրատիվ (բուժիչ) Բիսֆոսֆոնատով թերապիա",
        "Կուրատիվ (բուժիչ) Սիմպտոմատիկ",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->treatments as $treatment) {
            TreatmentList::create(["name" => $treatment]);
        }
    }
}
