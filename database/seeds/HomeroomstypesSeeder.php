<?php
use App\Models\Roomtypes;
use Illuminate\Database\Seeder;

class HomeroomstypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roomtypes_json = file_get_contents('./database/seeds/dumps/homeroomtypes.json');
        $roomtypes = json_decode($roomtypes_json, true);

        foreach ($roomtypes as $item) {
            Roomtypes::create($item);
        }
    }
}
