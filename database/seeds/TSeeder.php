<?php

use Illuminate\Database\Seeder;

use App\Models\T;

class TSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t_json = file_get_contents('./database/seeds/dumps/tnm/t.json');
        $t_array = json_decode($t_json, true);

        foreach ($t_array as $item) {
            T::create($item);
        }
    }
}
