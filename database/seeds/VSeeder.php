<?php

use Illuminate\Database\Seeder;
use App\Models\V;

class VSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $v_json = file_get_contents('./database/seeds/dumps/tnm/v.json');
        $v_array = json_decode($v_json, true);

        foreach ($v_array as $item) {
            V::create($item);
        }
    }
}
