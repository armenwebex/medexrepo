<?php

use Illuminate\Database\Seeder;
use App\Models\TreatmentWasCarriedOutList;

class TreatmentWasCarriedOutListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['code' => '', 'name' => 'Ստացիոնար (շուրջօրյա հսկողություն պահանջող)'],
            ['code' => '', 'name' => 'Ցերեկային ստացիոնար (շուրջօրյա հսկողություն պահանջող)'],
            ['code' => '', 'name' => 'Ամբուլատոր (շուրջօրյա հսկողություն չպահանջող)'],
        ];

        foreach ($data as $value) {
            TreatmentWasCarriedOutList::create($value);
        }
    }
}
