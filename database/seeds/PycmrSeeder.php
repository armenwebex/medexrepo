<?php

use Illuminate\Database\Seeder;
use App\Models\Pycmr;

class PycmrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pycmr_json = file_get_contents('./database/seeds/dumps/tnm/pycmr.json');
        $pycmr_array = json_decode($pycmr_json, true);

        foreach ($pycmr_array as $item) {
            Pycmr::create($item);
        }
    }
}
